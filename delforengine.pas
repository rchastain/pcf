
unit DelForEngine;

interface

uses
  SysUtils, OObjects, Classes, DelForTypes;

type
  TPascalWord = class
  private
    procedure SetCase(ACase: TCase); virtual;
    function GetCase: TCase; virtual;
    procedure SetExpression(AExpression: pchar); virtual;
    function GetExpression: pchar; virtual;
  public
    constructor Create;
    function WordType: TWordType; virtual;
    procedure GetLength(var Length: integer); virtual;
    function Space(SpaceBefore: TSpaceBefore): boolean; virtual;
    function ReservedType: TReservedType; virtual;
    procedure SetSpace(SpaceBefore: TSpaceBefore; State: boolean); virtual;
    procedure SetReservedType(AReservedType: TReservedType); virtual;
    function GetEString(Dest: pchar): pchar; virtual; abstract;
    function ChangeComment(commchar: char): boolean;
    property Expression: pchar read GetExpression write SetExpression;
    property ExpressionCase: TCase read GetCase write SetCase;
  end;

  TLineFeed = class(TPascalWord)
  public
    nSpaces: integer;
    oldnSpaces: integer;
    wrapped: boolean;
    constructor Create(AOldnSpaces: integer);
    procedure SetIndent(n: integer);
    procedure IncIndent(n: integer);
    procedure GetLength(var Length: integer); override;
    function ReservedType: TReservedType; override;
    function GetEString(Dest: pchar): pchar; override;
  end;

  { TExpression }

  TExpression = class(TPascalWord)
  private
    procedure SetCase(ACase: TCase); override;
    function GetCase: TCase; override;
    procedure SetExpression(AExpression: pchar); override;
  public
    FExpression: pchar;
    FWordType: TWordType;
    FFormatType: byte;
    FReservedType: TReservedType;
    constructor Create(AType: TWordType; AExpression: pchar);
    procedure CheckReserved;
    procedure SetSpace(SpaceBefore: TSpaceBefore; State: boolean); override;
    procedure SetReservedType(AReservedType: TReservedType); override;
    function Space(SpaceBefore: TSpaceBefore): boolean; override;
    function GetEString(Dest: pchar): pchar; override;
    procedure GetLength(var Length: integer); override;
    function GetExpression: pchar; override;
    function WordType: TWordType; override;
    function ReservedType: TReservedType; override;
    destructor Destroy; override;
  end;

  TAlignExpression = class(TExpression)
  public
    AlignPos: byte;
    nSpaces: byte;
    constructor Create(Like: TExpression; Pos: byte);
    procedure GetLength(var Length: integer); override;
    function GetEString(Dest: pchar): pchar; override;
  end;

  TDelForParser = class(TObject)
  private
    FSettings: TSettings;
    FileText: TOCollection;
    FCurrentText: pchar;
    FCapNames: TKeywordColl;
    nIndent: integer;
    ProcLevel: integer;
    ReadingAsm: boolean;
    AsmComment: TWordType;
    prev: TPascalWord;
    PrevLine: TLineFeed;
    prevType: TWordType;
    FOnProgress: TProgressEvent;
    HasAligned: boolean;
    LeftPointBracket: integer;
    procedure SetFillNewWords(AFillNewWords: TFillMode);
    function AlignExpression(I: integer; aPos: integer): TPascalWord;
    procedure CheckPrintable(P: pchar);
    procedure ReadAsm(var Buff: pchar);
    function ReadHalfComment(Dest: pchar; var Source: pchar): TWordType;
    function ReadWord(Dest: pchar; var Source: pchar): TWordType;
    procedure SetTextStr(AText: pchar);
    function GetTextStr: pchar;
    procedure CheckWrapping;
    function GetString(Dest: pchar; var I: integer): pchar;
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadFromFile(AFileName: pchar);
    procedure LoadFromList(AList: TStringList);
    procedure LoadCapFile(ACapFile: pchar);
    procedure SaveCapFile(ACapFile: pchar);
    function Parse: boolean;
    procedure Clear;
    procedure WriteToFile(AFileName: pchar);
    procedure Add(Buff: pchar);
    property Text: pchar read GetTextStr write SetTextStr;
    property OnProgress: TProgressEvent read FOnProgress write FOnProgress;
    property CapNames: TKeywordColl read FCapNames write FCapNames;
    property Settings: TSettings read FSettings write FSettings;
    property SpacePerIndent: byte read FSettings.SpacePerIndent write FSettings.SpacePerIndent;
    property SpaceOperators: TSpaceBefore read FSettings.SpaceOperators write FSettings.SpaceOperators;
    property SpaceEqualOper: TSpaceBefore read FSettings.SpaceEqualOper write FSettings.SpaceEqualOper;
    property SpaceColon: TSpaceBefore read FSettings.SpaceColon write FSettings.SpaceColon;
    property SpaceComma: TSpaceBefore read FSettings.SpaceComma write FSettings.SpaceComma;
    property SpaceSemiColon: TSpaceBefore read FSettings.SpaceSemiColon write FSettings.SpaceSemiColon;
    property SpaceLeftBr: TSpaceBefore read FSettings.SpaceLeftBr write FSettings.SpaceLeftBr;
    property SpaceRightBr: TSpaceBefore read FSettings.SpaceRightBr write FSettings.SpaceRightBr;
    property SpaceLeftHook: TSpaceBefore read FSettings.SpaceLeftHook write FSettings.SpaceLeftHook;
    property SpaceRightHook: TSpaceBefore read FSettings.SpaceRightHook write FSettings.SpaceRightHook;
    property UpperCompDirectives: boolean read FSettings.UpperCompDirectives write FSettings.UpperCompDirectives;
    property UpperNumbers: boolean read FSettings.UpperNumbers write FSettings.UpperNumbers;
    property ReservedCase: TCase read FSettings.ReservedCase write FSettings.ReservedCase;
    property StandDirectivesCase: TCase read FSettings.StandDirectivesCase write FSettings.StandDirectivesCase;
    property ChangeIndent: boolean read FSettings.ChangeIndent write FSettings.ChangeIndent;
    property IndentBegin: boolean read FSettings.IndentBegin write FSettings.IndentBegin;
    property NoIndentElseIf: boolean read FSettings.NoIndentElseIf write FSettings.NoIndentElseIf;
    property IndentComments: boolean read FSettings.IndentComments write FSettings.IndentComments;
    property IndentCompDirectives: boolean read FSettings.IndentCompDirectives write FSettings.IndentCompDirectives;
    property IndentTry: boolean read FSettings.IndentTry write FSettings.IndentTry;
    property IndentTryElse: boolean read FSettings.IndentTryElse write FSettings.IndentTryElse;
    property IndentCaseElse: boolean read FSettings.IndentCaseElse write FSettings.IndentCaseElse;
    property BlankProc: boolean read FSettings.BlankProc write FSettings.BlankProc;
    property RemoveDoubleBlank: boolean read FSettings.RemoveDoubleBlank write FSettings.RemoveDoubleBlank;
    property FeedRoundBegin: TFeedBegin read FSettings.FeedRoundBegin write FSettings.FeedRoundBegin;
    property FeedAfterThen: boolean read FSettings.FeedAfterThen write FSettings.FeedAfterThen;
    property ExceptSingle: boolean read FSettings.FeedAfterThen write FSettings.ExceptSingle;
    property NoFeedBeforeThen: boolean read FSettings.NoFeedBeforeThen write FSettings.NoFeedBeforeThen;
    property FeedElseIf: boolean read FSettings.FeedElseIf write FSettings.FeedElseIf;
    property FeedEachUnit: boolean read FSettings.FeedEachUnit write FSettings.FeedEachUnit;
    property FeedAfterVar: boolean read FSettings.FeedAfterVar write FSettings.FeedAfterVar;
    property WrapLines: boolean read FSettings.WrapLines write FSettings.WrapLines;
    property WrapPosition: byte read FSettings.WrapPosition write FSettings.WrapPosition;
    property AlignCommentPos: byte read FSettings.AlignCommentPos write FSettings.AlignCommentPos;
    property AlignComments: boolean read FSettings.AlignComments write FSettings.AlignComments;
    property AlignVarPos: byte read FSettings.AlignVarPos write FSettings.AlignVarPos;
    property AlignVar: boolean read FSettings.AlignVar write FSettings.AlignVar;
    property FeedBeforeEnd: boolean read FSettings.FeedBeforeEnd write FSettings.FeedBeforeEnd;
    property FillNewWords: TFillMode read FSettings.FillNewWords write SetFillNewWords;
    property FeedAfterSemiColon: boolean read FSettings.FeedAfterSemiColon write FSettings.FeedAfterSemiColon;
    property BlankSubProc: boolean read FSettings.BlankSubProc write FSettings.BlankSubProc;
    property StartCommentOut: TCommentArray read FSettings.StartCommentOut write FSettings.StartCommentOut;
    property EndCommentOut: TCommentArray read FSettings.EndCommentOut write FSettings.EndCommentOut;
    property SupportCOperators: boolean read FSettings.SupportCOperators write FSettings.SupportCOperators;
  end;
  
var
  DelForParser: TDelForParser = nil;

implementation

uses
  Log;

const
  CLogName = 'pcf_delforengine.log';
  
var
  LLog: TLog;
  
constructor TDelForParser.Create;
begin
  LLog.Append('TDelForParser.Create');
  DelForParser := Self;
  CapNames := TKeywordColl.Create(10);
  Clear;
end;

function TDelForParser.AlignExpression(I: integer; aPos: integer): TPascalWord;
var
  OldExpr: TExpression;
begin
  LLog.Append(Format('TDelForParser.AlignExpression(%d,%d)', [I, aPos]));
  HasAligned := True;
  with FileText do
  begin
    OldExpr := TExpression(Items[I]);
    result := TAlignExpression.Create(OldExpr, aPos);
    Items[I] := result;
    OldExpr.Free;
  end;
end;

procedure TDelForParser.Clear;
begin
  LLog.Append('TDelForParser.Clear');
  HasAligned := False;
  LeftPointBracket := 0;
  nIndent := 0;
  ReadingAsm := False;
  PrevLine := nil;
  prev := nil;
  prevType := wtNothing;
  if FileText = nil then
    FileText := TOCollection.Create(500)
  else
    FileText.FreeAll;
end;

procedure Nop();
begin
end;

procedure TDelForParser.Add(Buff: pchar);
var
  AWord: array[0..Maxline] of char;
begin
  LLog.Append(Format('TDelForParser.Add(%s)', [Buff]));
  PrevLine := TLineFeed.Create(0);
  FileText.Add(PrevLine);
  if ReadingAsm then
    ReadAsm(Buff);
  while (Buff^ <> #0) do
  begin
    case prevType of
      wtHalfComment, wtHalfStarComment, wtHalfOutComment:
        prevType := ReadHalfComment(AWord, Buff);
    else
      prevType := ReadWord(AWord, Buff);
    end;
    if not (prevType = wtSpaces) then
    begin
      FileText.Add(TExpression.Create(prevType, AWord));
      if ReadingAsm and (Buff^ <> #0) then
        ReadAsm(Buff);
    end else
      if (PrevLine <> nil) and (PrevLine.nSpaces = 0) then
      begin
        PrevLine.nSpaces := StrLen(AWord);
        PrevLine.oldnSpaces := StrLen(AWord);
      end;
  end;
end;

procedure TDelForParser.LoadFromFile(AFileName: pchar);
var
  InFile: TextFile;
  Buff: array[0..Maxline] of char;
begin
  LLog.Append(Format('TDelForParser.LoadFromFile(%s)', [AFileName]));
  if Assigned(OnProgress) then
    OnProgress(Self, 0);
  PrevLine := nil;
  ReadingAsm := False;
  AssignFile(InFile, AFileName);
  try
    Reset(InFile);
    try
      while not Eof(InFile) and (FileText.Count < MaxCollectionSize - 100) do
      begin
        Readln(InFile, Buff);
        Add(Buff);
      end;
      if FileText.Count >= MaxCollectionSize - 100 then
        raise Exception.Create('File to large to reformat')
    finally
      CloseFile(InFile);
    end;
  finally
  end;
  if Assigned(OnProgress) then
    OnProgress(Self, 33);
end;

procedure TDelForParser.LoadFromList(AList: TStringList);
var
  Buff: array[0..Maxline] of char;
  I, k: integer;
begin
  LLog.Append('TDelForParser.LoadFromList');
  if Assigned(OnProgress) then
    OnProgress(Self, 0);
  PrevLine := nil;
  k := 0;
  ReadingAsm := False;
  with AList do
    if Count = 0 then
      Self.Add(StrCopy(Buff, ''))
    else
      for I := 0 to Count - 1 do
      begin
        StrCopy(Buff, pchar(Strings[I]));
        Self.Add(Buff);
        if Assigned(OnProgress) then
        begin
          Inc(k);
          if k = 20 then
          begin
            k := 0;
            OnProgress(Self, Round(I / Count * 34));
          end;
        end;
      end;
end;

procedure TDelForParser.ReadAsm(var Buff: pchar);
var
  P, P1: pchar;
begin
  LLog.Append('TDelForParser.ReadAsm');
  P := Buff;
  P1 := Buff;
  while P1^ = ' ' do
    Inc(P1);
  repeat
    CheckPrintable(P);
    case AsmComment of
      wtHalfComment:
        begin
          if P^ = '}' then
            AsmComment := wtWord;
          Inc(P);
        end;
      wtHalfStarComment:
        begin
          if StrLComp(P, '*)', 2) = 0 then
          begin
            AsmComment := wtWord;
            Inc(P);
          end;
          Inc(P);
        end;
    else
      if (StrLIComp(P, 'end', 3) = 0) and ((P = Buff) or ((P - 1)^ in [' ', Tab]) and ((P + 3)^ in [#0, ';', ' ', Tab])) then
      begin
        ReadingAsm := False;
        if P1 <> P then
        begin
          Dec(P);
          P^ := #0;
          FileText.Add(TExpression.Create(wtAsm, P1));
          P^ := ' ';
          Inc(P);
        end;
        Buff := P;
        Exit;
      end else
        if P^ = '{' then
        begin
          while (P^ <> '}') and (P^ <> #0) do
          begin
            Inc(P);
            CheckPrintable(P);
          end;
          if P^ <> '}' then
            AsmComment := wtHalfComment;
        end else
          if StrLComp(P, '(*', 2) = 0 then
          begin
            while (StrLComp(P, '*)', 2) <> 0) and (P^ <> #0) do
            begin
              Inc(P);
              CheckPrintable(P);
            end;
            if StrLComp(P, '*)', 2) <> 0 then
              AsmComment := wtHalfStarComment;
          end else
            if StrLComp(P, '//', 2) = 0 then
              while P^ <> #0 do
              begin
                CheckPrintable(P);
                Inc(P);
              end
            else
              Inc(P);
    end;
  until (P^ = #0);
  FileText.Add(TExpression.Create(wtAsm, Buff));
  Buff := P;
end;

procedure TDelForParser.CheckPrintable(P: pchar);
begin
  LLog.Append(Format('TDelForParser.CheckPrintable(%s)', [P]));
  if (P <> nil) and (P^ in NotPrintable) then
  begin
    while (P^ in NotPrintable) and not (StrLComp(P, #13#10, 2) = 0) do
    begin
      P^ := ' ';
      Inc(P);
    end;
  end;
end;

function TDelForParser.ReadWord(Dest: pchar; var Source: pchar): TWordType;
const
  operators = '+-*/=<>[].,():;{}@^';
  AllOper = operators + ' {}'''#9;
var
  P: pchar;
  Len: integer;

  procedure ReadString;

    procedure ReadQuotes;
    begin
      while (P^ = '''') and ((P + 1)^ = '''') do
        Inc(P, 2);
    end;
    
  begin
    repeat
      Inc(P);
      CheckPrintable(P);
      if P^ = '''' then
      begin
        ReadQuotes;
        if ((P + 1)^ = '#') then
        begin
          Inc(P);
          while P^ in ['0'..'9', 'A'..'F', 'a'..'f', '$', '#', '^'] do
            Inc(P);
          if P^ = '''' then
            Inc(P)
          else
          begin
            Dec(P);
            Exit;
          end;
          ReadQuotes;
        end else
          if (P + 1)^ = '^' then
          begin
            Inc(P);
            while P^ in ['0'..'9', 'A'..'Z', 'a'..'z', '$', '#', '^'] do
              Inc(P);
            if P^ = '''' then
              Inc(P)
            else
            begin
              Dec(P);
              Exit;
            end;
            ReadQuotes;
          end;
      end;
    until (P^ = '''') or (P^ = #0);
  end;

  procedure ReadIdentifier;
  begin
    result := wtWord;
    while (StrScan(AllOper, P^) = nil) and (P^ <> #0) do
      Inc(P);
    Dec(P);
  end;
  
begin
  LLog.Append(Format('TDelForParser.ReadWord(%s,)', [Dest]));
  P := Source;
  CheckPrintable(P);
  if P^ in [Tab, ' '] then
  begin
    result := wtSpaces;
    while (P^ in [Tab, ' ']) do
      Inc(P);
    Dec(P);
  end else
    if (Settings.StartCommentOut[0] <> #0)
    and (Settings.EndCommentOut[0] <> #0)
    and (StrLIComp(P, Settings.StartCommentOut, StrLen(Settings.StartCommentOut)) = 0) then
    begin
      result := wtHalfOutComment;
      Inc(P, StrLen(Settings.StartCommentOut));
      Len := StrLen(Settings.EndCommentOut);
      while (StrLIComp(P, Settings.EndCommentOut, Len) <> 0) and (P^ <> #0) do
      begin
        Inc(P);
        CheckPrintable(P);
      end;
      if StrLIComp(P, Settings.EndCommentOut, Len) = 0 then
      begin
        Inc(P, Len - 1);
        result := wtFullOutComment;
      end;
    end else
      if P^ = '{' then
      begin
        result := wtHalfComment;
        while (P^ <> '}') and (P^ <> #0) do
        begin
          Inc(P);
          CheckPrintable(P);
        end;
        if (P^ = '}') then
        begin
          result := wtFullComment;
          if (Source + 1)^ = '$' then
            result := wtCompDirective;
        end;
      end else
        if StrLComp(P, '(*', 2) = 0 then
        begin
          result := wtHalfStarComment;
          while (StrLComp(P, '*)', 2) <> 0) and (P^ <> #0) do
          begin
            Inc(P);
            CheckPrintable(P);
          end;
          if StrLComp(P, '*)', 2) = 0 then
          begin
            Inc(P);
            result := wtFullComment;
          end;
        end else
          if StrLComp(P, '//', 2) = 0 then
          begin
            result := wtFullComment;
            while P^ <> #0 do
            begin
              CheckPrintable(P);
              Inc(P);
            end
          end else
            if P^ = '''' then
            begin
              result := wtString;
              ReadString;
              if P^ = #0 then
                result := wtErrorString;
            end else
              if P^ = '^' then
              begin
                if ((P + 1)^ in ['a'..'z', 'A'..'Z']) and ((P + 2)^ in ['''', '^', '#']) then
                begin
                  result := wtString;
                  while P^ in ['0'..'9', 'A'..'Z', 'a'..'z', '$', '#', '^'] do
                    Inc(P);
                  if P^ = '''' then
                    ReadString;
                end else
                  result := wtOperator;
              end else
                if StrScan(operators, P^) <> nil then
                begin
                  result := wtOperator;
                  if StrLComp(P, '<=', 2) = 0 then
                    Inc(P);
                  if StrLComp(P, '>=', 2) = 0 then
                    Inc(P);
                  if StrLComp(P, '<>', 2) = 0 then
                    Inc(P);
                  if StrLComp(P, ':=', 2) = 0 then
                    Inc(P);
                  if SupportCOperators then
                  begin
                    if StrLComp(P, '+=', 2) = 0 then
                      Inc(P);
                    if StrLComp(P, '-=', 2) = 0 then
                      Inc(P);
                    if StrLComp(P, '*=', 2) = 0 then
                      Inc(P);
                    if StrLComp(P, '/=', 2) = 0 then
                      Inc(P);
                  end;
                  if StrLComp(P, '..', 2) = 0 then
                    Inc(P);
                  if StrLComp(P, '(.', 2) = 0 then
                  begin
                    Inc(LeftPointBracket);
                    Inc(P);
                  end;
                  if StrLComp(P, '.)', 2) = 0 then
                  begin
                    Dec(LeftPointBracket);
                    Inc(P);
                  end;
                end else
                  if P^ = '$' then
                  begin
                    result := wtHexNumber;
                    Inc(P);
                    while UpCase(P^) in ['0'..'9', 'A'..'F'] do
                      Inc(P);
                    Dec(P);
                  end else
                    if P^ = '#' then
                    begin
                      result := wtNumber;
                      while P^ in ['0'..'9', 'A'..'F', 'a'..'f', '$', '#', '^'] do
                        Inc(P);
                      if P^ = '''' then
                      begin
                        result := wtString;
                        ReadString;
                      end
                      else
                        Dec(P);
                    end else
                      if P^ in ['0'..'9'] then
                      begin
                        result := wtNumber;
                        while (P^ in ['0'..'9', '.']) and not (StrLComp(P, '..',
                          2) = 0)
                          and not ((LeftPointBracket > 0) and (StrLComp(P, '.)',
                          2) = 0)) do
                          Inc(P);
                        if UpCase(P^) = 'E' then
                          if (P + 1)^ in ['0'..'9', '-', '+'] then
                          begin
                            Inc(P, 2);
                            while (P^ in ['0'..'9']) do
                              Inc(P);
                          end;
                        Dec(P);
                      end else
                        ReadIdentifier;
  
  StrLCopy(Dest, Source, P - Source + 1);
  if (StrIComp(Dest, 'asm') = 0) then
  begin
    ReadingAsm := True;
    AsmComment := wtWord;
  end;
  if P^ = #0 then
    Source := P
  else
  begin
    if (P + 1)^ in [Tab, ' '] then
      Inc(P);
    Source := P + 1;
  end;
end;

function TDelForParser.ReadHalfComment(Dest: pchar; var Source: pchar): TWordType;
var
  Len: integer;
  P: pchar;
begin
  LLog.Append(Format('TDelForParser.ReadHalfComment(%s,)', [Dest]));
  P := Source;
  while P^ in [Tab, ' '] do
    Inc(P);
  if (PrevLine <> nil) and (PrevLine.nSpaces = 0) then
  begin
    PrevLine.nSpaces := P - Source;
    PrevLine.oldnSpaces := P - Source;
    P := StrCopy(Source, P);
  end;
  ReadHalfComment := prevType;
  if prevType = wtHalfComment then
  begin
    while (P^ <> '}') and (P^ <> #0) do
      Inc(P);
    if (P^ = '}') then
    begin
      ReadHalfComment := wtFullComment;
      Inc(P);
    end;
  end else
    if prevType = wtHalfStarComment then
    begin
      while (StrLComp(P, '*)', 2) <> 0) and (P^ <> #0) do
        Inc(P);
      if StrLComp(P, '*)', 2) = 0 then
      begin
        ReadHalfComment := wtFullComment;
        Inc(P, 2);
      end;
    end else
    begin
      Len := StrLen(Settings.EndCommentOut);
      while (StrLIComp(P, Settings.EndCommentOut, Len) <> 0) and (P^ <> #0) do
        Inc(P);
      if StrLIComp(P, Settings.EndCommentOut, Len) = 0 then
      begin
        ReadHalfComment := wtFullOutComment;
        Inc(P, Len);
      end;
    end;
  StrLCopy(Dest, Source, P - Source);
  if P^ = #0 then
    Source := P
  else
  begin
    if (P^ in [Tab, ' ']) then
      Inc(P);
    Source := P;
  end;
end;

function TDelForParser.Parse: boolean;
type
  TRec = record
    RT: TReservedType;
    nInd: integer;
  end;
const
  MaxStack = 150;
type
  TStackArray = array[0..MaxStack] of TRec;
  TStackStackRec = record
    stackptr: integer;
    ProcLevel: integer;
    stack: TStackArray;
    nIndent: integer;
  end;

var
  Prev1: TPascalWord;
  OldWrapIndent: boolean;
  PrevPrevLine: TLineFeed;
  stack: TStackArray;
  stackptr: integer;
  StackStack: array[0..2] of TStackStackRec;
  StackStackPtr: integer;
  WrapIndent: boolean;
  interfacePart: boolean;
  NTmp: integer;
  PrevOldNspaces: integer;
  I, J: integer;

  function GetStackTop: TReservedType;
  begin
    if stackptr >= 0 then
      GetStackTop := stack[stackptr].RT
    else
      GetStackTop := rtNothing;
  end;

  procedure SetSpacing(PascalWord, prev: TPascalWord; I: integer);
  var
    Prev2: TPascalWord;
    rtype: TReservedType;
    wType: TWordType;
    k: integer;
    S: array[0..Maxline] of char;
    Found: boolean;
  begin
    if PascalWord <> nil then
    begin
      rtype := PascalWord.ReservedType;
      wType := PascalWord.WordType;
     {if (rType = rtPrivate) and (prev <> nil) and (prev.ReservedType <> rtLineFeed) then
      begin
        PascalWord.SetReservedType(rtNothing);
        rType := rtNothing;
      end;}
      if not (rtype in NoReservedTypes) then
        PascalWord.ExpressionCase := ReservedCase
      else
        if rtype in StandardDirectives then
          PascalWord.ExpressionCase := StandDirectivesCase
        else
        begin
          PascalWord.ExpressionCase := rfUnchanged;
          if (wType = wtWord) and (PascalWord.Expression <> nil) then
          begin
            Found := False;
            if (FillNewWords in [fmAddNewWord, fmAddUse, fmAddUseExcept]) and (rtype in (NoReservedTypes - StandardDirectives)) then
            begin
              Found := CapNames.Search(PascalWord.Expression, I);
              if not Found then
              begin
                StrCopy(S, '*');
                StrCat(S, PascalWord.Expression); {comment out}
                if not CapNames.Search(@S, J) then
                  CapNames.Insert(I, StrNew(PascalWord.Expression));
              end;
            end;
            if (FillNewWords in [fmUse, fmAddUse]) or ((FillNewWords in [fmExceptDirect, fmAddUseExcept]) and not (rtype in StandardDirectives)) then
            begin
              if not Found then
                Found := CapNames.Search(PascalWord.Expression, I);
              if Found then
              begin
                PascalWord.Expression := CapNames.Items[I];
                PascalWord.ExpressionCase := rfUnchanged;
              end;
            end;
          end;
        end;

      case rtype of
        rtHelper, rtThen, rtOf, rtElse, rtDo, rtAsm:
          PascalWord.SetSpace(spBoth, True);
        rtEnd, rtFuncDirective:
          PascalWord.SetSpace(spBefore, True);
        rtIf, rtUntil, rtWhile, rtCase, rtRecord:
          PascalWord.SetSpace(spAfter, True);
        rtOper, rtMathOper, rtPlus, rtMinus, rtLogOper, rtEquals:
          PascalWord.SetSpace(SpaceOperators, True);
        rtEqualOper:
          PascalWord.SetSpace(SpaceEqualOper, True);
        rtColon:
          PascalWord.SetSpace(SpaceColon, True);
        rtSemiColon:
          PascalWord.SetSpace(SpaceSemiColon, True);
        rtComma:
          PascalWord.SetSpace(SpaceComma, True);
        rtLeftBr:
          begin
            PascalWord.SetSpace(SpaceLeftBr, True);
            if prev.ReservedType = rtLeftBr then
              PascalWord.SetSpace(spBefore, False);
          end;
        rtLeftHook:
          begin
            PascalWord.SetSpace(SpaceLeftHook, True);
            if prev.ReservedType = rtLeftHook then
              PascalWord.SetSpace(spBefore, False);
          end;
        rtRightBr:
          PascalWord.SetSpace(SpaceRightBr, True);
        rtRightHook:
          PascalWord.SetSpace(SpaceRightHook, True);
      end;
      
      if (wType in [wtNumber, wtHexNumber]) and UpperNumbers then
        PascalWord.SetCase(rfUpperCase);
      
      if (prev <> nil) then
      begin
        if (SpaceOperators in [spBoth, spBefore, spAfter]) and (wType in [wtString, wtFullComment, wtHalfComment, wtHalfStarComment]) and not (prev.ReservedType in [rtDotDot, rtLineFeed]) then
          PascalWord.SetSpace(spBefore, True);
        if (rtype in [rtMinus, rtPlus]) then
        begin
          Prev2 := prev;
          k := 0;
          while (Prev2 <> nil) and (Prev2.ReservedType in [rtComment, rtLineFeed]) do
          begin
            Inc(k);
            if k > I then
              Prev2 := nil
            else
              Prev2 := TPascalword(FileText.Items[I - k]);
          end;
          if (Prev2 <> nil) and (Prev2.ReservedType in [rtOper, rtMathOper, rtPlus, rtMinus, rtSemiColon, rtOf, rtLogOper, rtEquals, rtEqualOper, rtLeftBr, rtLeftHook, rtComma, rtDefault]) then
            PascalWord.SetSpace(spAfter, False);
        end;
        if (rtype = rtLeftHook) then
        begin
          if not (prev.ReservedType in [rtReserved, rtNothing, rtRightBr, rtRightHook]) then
            PascalWord.SetSpace(spBefore, True);
        end;
        if PascalWord.Space(spBefore) and (prev.ReservedType in [rtLeftBr, rtLeftHook, rtLineFeed]) then
          PascalWord.SetSpace(spBefore, False);
        if (prev.WordType in [wtWord, wtNumber, wtHexNumber, wtString]) and (wType in [wtWord, wtNumber, wtHexNumber]) then
          PascalWord.SetSpace(spBefore, True);
        if (prev.ReservedType = rtComment) and (wType in [wtWord, wtNumber, wtHexNumber]) then
          prev.SetSpace(spAfter, True);
        if PascalWord.Space(spBefore) and prev.Space(spAfter) then
          prev.SetSpace(spAfter, False); {avoid double spaces}
      end;
    end;
  end;

  procedure SetPrevIndent;
  begin
    if PrevLine <> nil then
      PrevLine.SetIndent(nIndent + NTmp + ProcLevel);
  end;

  procedure Push(R: TReservedType; n, ninc: integer);
  begin
    Inc(stackptr);
    if stackptr > MaxStack then
      raise EFormatException.Create('Stack overflow');
    with stack[stackptr] do
    begin
      RT := R;
      nInd := n;
      nIndent := n + ninc;
    end;
  end;

  function HasType(AType: TReservedType): boolean;
  var
    I: integer;
  begin
    HasType := False;
    for I := 0 to stackptr do
      if stack[I].RT = AType then
      begin
        HasType := True;
        Exit;
      end;
  end;

  function Pop: TReservedType;
  begin
    if stackptr >= 0 then
    begin
      nIndent := stack[stackptr].nInd;
      if (stack[stackptr].RT = rtProcedure) and (ProcLevel > 0) then
        Dec(ProcLevel);
      Pop := stack[stackptr].RT;
      Dec(stackptr);
    end else
    begin
      nIndent := 0;
      ProcLevel := 0;
      Pop := rtNothing;
    end;
  end;

  function GetWord(I: integer): TPascalWord;
  begin
    with FileText do
      if (I < Count) and (I >= 0) then
        GetWord := TPascalWord(Items[I])
      else
        GetWord := nil;
  end;

  function GetNextNoComment(var I, k: integer): TPascalWord;
  begin
    k := 0;
    repeat
      Inc(k);
      result := GetWord(I + k);
    until (result = nil) or (result.ReservedType <> rtComment);
  end;

  function InsertBlankLines(atIndex, NLines: integer): TLineFeed;
  var
    J: integer;
    AfterWord: TPascalWord;
  begin
    result := PrevLine;
    for J := 0 to NLines - 1 do
    begin
      result := TLineFeed.Create(0);
      result.SetIndent(nIndent);
      AfterWord := TPascalWord(FileText.Items[atIndex]);
      if AfterWord.Space(spBefore) then
        AfterWord.SetSpace(spBefore, False);
      FileText.Insert(atIndex, result);
      SetSpacing(AfterWord, result, atIndex);
    end;
    if atIndex <= I then
      Inc(I, NLines);
  end;

  procedure CheckBlankProc;
  var
    k: integer;
    Prev2: TPascalWord;
  begin
    if (prev <> nil) then
    begin
      k := 1;
      if prev.ReservedType = rtClass then
      begin
        k := 2;
        Prev2 := GetWord(I - 2);
      end
      else
        Prev2 := prev;
      if (Prev2 <> nil) and (Prev2.ReservedType <> rtLineFeed) then
      begin
        PrevLine := InsertBlankLines(I - k, 2);
        prev := PrevLine;
      end
      else
      begin
        Inc(k);
        Prev2 := GetWord(I - k);
        if (Prev2 <> nil) and (Prev2.ReservedType <> rtLineFeed) then
        begin
          PrevLine := InsertBlankLines(I - k + 1, 1);
          prev := PrevLine;
        end;
      end;
    end;
  end;

  procedure PutCommentBefore(aComment: pchar);
  var
    J: integer;
    P: TPascalWord;
  begin
    J := I - 2;
    P := GetWord(J);
    if P.ReservedType = rtComment then
      P.Expression := aComment
    else
    begin
      P := TExpression.Create(wtWord, aComment);
      P.SetReservedType(rtComment);
      FileText.Insert(I, P);
      Inc(I);
      P := TLineFeed.Create(0);
      TLineFeed(P).SetIndent(nIndent);
      FileText.Insert(I, P);
      Inc(I);
    end;
  end;

  function MakeLineFeed(k, J: integer): TLineFeed;
  var
    next: TPascalWord;
  begin
    next := GetNextNoComment(k, J);
    if (next <> nil) and (next.ReservedType <> rtLineFeed) then
      result := InsertBlankLines(k + J, 1)
    else
      result := PrevLine;
  end;

  procedure SetTopIndent;
  begin
    if stackptr >= 0 then
    begin
      nIndent := stack[stackptr].nInd;
      SetPrevIndent;
    end;
  end;

  procedure DecPrevIndent;
  begin
    if PrevLine <> nil then
      PrevLine.IncIndent(-1);
  end;

  procedure CheckIndent(PascalWord: TPascalWord);
  var
    next: TPascalWord;
    lastPop: TReservedType;
    k: integer;
    P, P2: pchar;
    Top: TReservedType;
    functdeclare, NoBlankLine: boolean;
    rtype: TReservedType;
    wType: TWordType;
    procedure CheckSlashComment;
    var
      PasWord: TPascalWord;
      PrevPasWord: TPascalWord;
      Buff: array[0..200] of char;
    begin
      prev := GetWord(I - 1);
      if (prev <> nil) and (prev.ReservedType = rtComment) and (prev.Expression^ = '/') then {fix for situation with a // comment on prev line: begin becomes part of the comment}
        if not prev.ChangeComment('{') then
        begin
          k := 0;
          PasWord := nil;
          repeat
            PrevPasWord := PasWord;
            PasWord := GetWord(I + k);
            Inc(k);
          until (PasWord = nil) or (PasWord.ReservedType = rtLineFeed);
          Dec(k);
          if ((PrevPasWord.ReservedType = rtComment) and (PrevPasWord.Expression^ = '/')) then
          begin
            prev.Expression := StrCat(StrCat(StrCopy(Buff, '{'), prev.Expression + 2), '}');
            Exit;
          end else
            FileText.Delete(I - 1);
          FileText.Insert(I + k, prev);
          prev := GetWord(I - 1);
          SetSpacing(prev, GetWord(I - 2), I - 1);
          PascalWord := GetWord(I);
        end;
      PrevLine := PrevPrevLine;
    end;

    function NoBeginTryIndent: boolean;
    begin
      result := not ((indentBegin and (rtype = rtBegin)) or (IndentTry and (rtype = rtTry))) and (GetStackTop in [rtDo, rtThen, rtIfElse]);
    end;

    procedure CheckShortLine;
    begin
      k := 1;
      next := GetWord(I + k);
      if (next <> nil) and (next.ReservedType = rtLineFeed) then
      begin
        while not ((next = nil) or (next.ReservedType in [rtSemiColon, rtBegin, rtElse, rtDo, rtWhile, rtOn, rtThen, rtCase]) or ((k > 1) and (next.ReservedType = rtLineFeed))) do
        begin
          Inc(k);
          next := GetWord(I + k);
        end;
        if (next <> nil) and (next.ReservedType = rtSemiColon) then
        begin
          FileText.AtFree(I + 1);
        end;
      end;
    end;

    procedure ComplexIfElse;
    begin
      while (stackptr >= 0) and (lastPop <> rtThen) do
      begin
        lastPop := Pop;
        if lastPop = rtIfElse then
          ComplexIfElse;
      end;
      SetPrevIndent;
    end;
  begin
    if PascalWord <> nil then
    begin
      rtype := PascalWord.ReservedType;
      wType := PascalWord.WordType;
      if (rtype in [rtWhile, rtEnd, rtRepeat, rtBegin, rtUses, rtTry,
        rtProgram, rtType, rtvar,
          rtIf, rtThen, rtElse] + standardDirectives) and (prev <> nil) and
        (prev.ReservedType = rtDot) then
      begin
        PascalWord.SetReservedType(rtNothing);
        rtype := rtNothing;
      end;
      if rtype <> rtNothing then
      begin
        case rtype of
          rtIf:
            begin
              if FeedAfterThen and not FeedElseIf and (GetStackTop = rtIfElse) and (prev = PrevLine) then
              begin
                FileText.AtFree(I - 1);
                Dec(I);
                CheckSlashComment;
              end else
              begin
                if FeedElseIf and (prev <> PrevLine) then
                begin
                  PrevLine := MakeLineFeed(I - 1, 0);
                  prev := PrevLine;
                end;
              end;
              if ((prev <> nil) and (prev.ReservedType = rtElse)) or (NoIndentElseIf and (GetStackTop = rtIfElse)) then
              begin
                Pop;
                if GetStackTop = rtThen then
                  Pop;
                WrapIndent := True;
                Push(rtIfElse, nIndent, 0);
              end else
                Push(rtype, nIndent, 0);
            end;
          rtThen:
            if GetStackTop in [rtIf, rtIfElse] then
            begin
              WrapIndent := False;
              lastPop := Pop;
              if NoFeedBeforeThen and ((prev = PrevLine) and (TPascalWord(FileText.Items[I - 1]).ReservedType <> rtComment)) then
              begin
                FileText.AtFree(I - 1);
                Dec(I);
                CheckSlashComment;
              end;
              if FeedAfterThen then
              begin
                if MakeLineFeed(I, 1) <> PrevLine then
                begin
                  if (lastPop = rtIf) and Settings.ExceptSingle then
                    CheckShortLine;
                end;
              end;
              Push(rtype, nIndent, 1);
            end;
          rtColon:
            if GetStackTop = rtOf then
            begin
              Push(rtype, nIndent, 1);
              if FeedAfterThen then
              begin
                if (GetNextNoComment(I, k).ReservedType = rtBegin) and (MakeLineFeed(I, 1) <> PrevLine) then
                  CheckShortLine;
              end;
              WrapIndent := False;
            end else
              if GetStackTop = rtClassDecl then
              begin
                Pop;
                Push(rtClass, nIndent, 1);
              end else
                if AlignVar and (GetStackTop = rtVar) then
                  PascalWord := AlignExpression(I, AlignVarPos)
                else
                  if not (GetStackTop in [rtProcedure, rtProcDeclare]) then
                    WrapIndent := False;
          rtElse:
            begin
              lastPop := rtNothing;
              while (stackptr >= 0) and not (GetStackTop in [rtThen, rtOf, rtTry]) do
                lastPop := Pop;
              if lastPop = rtIfElse then
                ComplexIfElse;
              if FeedAfterThen then
              begin
                if (prev <> nil) and (GetWord(I - 1).ReservedType <> rtLineFeed) then
                begin
                  PrevLine := MakeLineFeed(I - 1, 0);
                  prev := PrevLine;
                end;
                if GetNextNoComment(I, k).ReservedType <> rtIf then
                  MakeLineFeed(I, 1);
              end;
              if stackptr >= 0 then
                nIndent := stack[stackptr].nInd;
              if prev = PrevLine then
                SetPrevIndent;
              if IndentTryElse and (GetStackTop = rtTry) then
              begin
                Inc(nIndent);
                SetPrevIndent;
              end else
                if IndentCaseElse and (GetStackTop = rtOf) then
                begin
                  Inc(nIndent);
                  SetPrevIndent;
                end;
              if GetStackTop = rtThen then
                Push(rtIfElse, nIndent, 1)
              else
                Push(rtype, nIndent, 1);
              WrapIndent := False;
            end;
          rtRepeat, rtRecord:
            begin
              Push(rtype, nIndent, 1);
              WrapIndent := False;
            end;
          rtClass:
            begin
              next := GetNextNoComment(I, k);
              if not ((next <> nil) and (next.ReservedType in [rtProcedure, rtProcDeclare, rtOf])) then
                {not a "class function" or "class of" declaration}
              begin
                WrapIndent := False;
                Push(rtClassDecl, nIndent, 1);
                {first assume that it is a class declaration
                 the first procedure replaces it with rtClass}
              end else
                PascalWord.SetSpace(spAfter, True);
            end;
          rtUntil:
            begin
              repeat
                lastPop := Pop;
              until (lastPop = rtRepeat) or (stackptr < 0);
              SetPrevIndent;
            end;
          rtLeftBr:
            if (GetStackTop = rtLeftBr) then
              Push(rtype, nIndent, 0)
            else
            begin
              OldWrapIndent := WrapIndent;
              if (ProcLevel <= 0) or (GetStackTop <> rtProcedure) then
                {niet erg netjes}
                Push(rtype, nIndent, 1)
              else
              begin
                k := 1;
                next := GetWord(I - k);
                while (I > k) and ((next <> nil) and (next.ReservedType in [rtDot, rtNothing])) do
                begin
                  Inc(k);
                  next := GetWord(I - k);
                end;
                if (next <> nil) and (next.ReservedType = rtProcedure) then
                  Push(rtype, nIndent, 0)
                else
                  Push(rtype, nIndent, 1);
              end;
              WrapIndent := False;
            end;
          rtLeftHook, rtWhile, rtOn:
            Push(rtype, nIndent, 0);
          rtRightBr:
            begin
              repeat
                lastPop := Pop;
              until (lastPop = rtLeftBr) or (stackptr < 0);
              if not (GetStackTop = rtLeftBr) then
                WrapIndent := OldWrapIndent;
            end;
          rtRightHook:
            begin
              repeat
                lastPop := Pop;
              until (lastPop = rtLeftHook) or (stackptr < 0);
              if GetStackTop = rtClassDecl {Interface} then
                WrapIndent := False;
            end;
          rtExcept:
            begin
              while (stackptr >= 0) and (GetStackTop <> rtTry) do
                Pop;
              SetTopIndent;
              Inc(nIndent);
              WrapIndent := False;
            end;
          rtPrivate:
            if not (GetStackTop in [rtClass, rtClassDecl]) then
              PascalWord.SetReservedType(rtNothing)
            else
              if prev.ReservedType = rtLineFeed then
              begin
                DecPrevIndent;
                WrapIndent := False;
              end;
          rtOf:
            begin
              case GetStackTop of
                rtCase:
                  begin
                    Push(rtype, nIndent, 1);
                    if FeedAfterThen then
                      MakeLineFeed(I, 1);
                    WrapIndent := False;
                  end;
                rtRecord: WrapIndent := False;
              end;
            end;
          rtLineFeed:
            begin
              if stackptr = -1 then
                WrapIndent := False;
              if RemoveDoubleBlank and (I >= 2) and (prev <> nil) and (prev = PrevLine) and (TLineFeed(FileText.Items[I - 2]) = PrevPrevLine) then
              begin
                FileText.AtFree(I - 2);
                Dec(I);
              end;
              next := GetNextNoComment(I, k);
              if (next <> nil) then
              begin
                if (next.ReservedType in [rtElse, rtIfElse, rtBegin, rtEnd, rtUntil, rtExcept]) then
                  WrapIndent := False;
                if WrapIndent then
                  NTmp := 1
                else
                  NTmp := 0;
                Top := GetStackTop;
                WrapIndent := True;
                if (next.ReservedType in [rtLineFeed]) or (Top in [rtUses, rtLeftBr]) then
                  WrapIndent := False;
              end;
              PrevPrevLine := PrevLine;
              PrevLine := TLineFeed(PascalWord);
              SetPrevIndent;
            end;
          rtAsm:
            begin
              while GetStackTop in [rtVar, rtType] do
                Pop;
              if GetStackTop = rtProcedure then
              begin
                Pop;
                DecPrevIndent;
              end;
              Push(rtype, nIndent, 0);
              with FileText do
              begin
                PascalWord := TPascalWord(Items[I]);
                while (I < Count - 1) and (PascalWord.ReservedType <>
                  rtEnd) do
                begin
                  if PascalWord.ReservedType = rtLineFeed then
                  begin
                    PrevLine := TLineFeed(PascalWord);
                    with PrevLine do
                      nSpaces := oldnSpaces;
                  end;
                  SetSpacing(PascalWord, prev, I);
                  Inc(I);
                  prev := PascalWord;
                  PascalWord := TPascalWord(Items[I]);
                end;
                if I < Count then
                  SetPrevIndent;
                Dec(I);
                Exit;
              end;
            end;
          rtComma:
            if FeedEachUnit and (GetStackTop = rtUses) then
            begin
              next := GetNextNoComment(I, k);
              if next.ReservedType <> rtLineFeed then
              begin
                MakeLineFeed(I, 0);
              end;
            end;
          rtProgram, rtUses, rtInitialization:
            if GetStackTop <> rtLeftBr then
            begin
              next := GetNextNoComment(I, k);
              if (rtype = rtUses) and (GetStackTop in [rtProcedure,
                rtProcDeclare, rtClass]) then
                PascalWord.SetReservedType(rtNothing)
              else
              begin
                DecPrevIndent;
                stackptr := -1;
                nIndent := 0;
                Push(rtype, 0, 1);
                WrapIndent := False;
              end;
            end;
          rtAbsolute:
            if not (GetStackTop in [rtVar, rtType]) then
              PascalWord.SetReservedType(rtNothing)
            else
            begin
              next := GetNextNoComment(I, k);
              if next.ReservedType = rtColon then
              begin
                DecPrevIndent;
                PascalWord.SetReservedType(rtNothing);
              end;
            end;
          rtFuncDirective, rtDefault:
            begin
              next := GetNextNoComment(I, k);
              if (next.ReservedType = rtColon) or not (GetStackTop in [rtProcedure, rtProcDeclare, rtClass]) or (prev.ReservedType in [rtProcedure, rtProcDeclare, rtDot]) then
                PascalWord.SetReservedType(rtNothing);
            end;
          rtForward:
            begin
              if GetStackTop in [rtProcedure, rtProcDeclare] then
                Pop
              else
                PascalWord.SetReservedType(rtNothing);
            end;
          rtProcedure:
            begin
              if GetStackTop = rtClassDecl then
              begin
                Pop;
                Push(rtClass, nIndent, 1);
              end;
              Prev1 := prev;
              J := I;
              if Prev1 <> nil then
              begin
                while (J > 0) and (Prev1.ReservedType in [rtComment, rtLineFeed]) do
                begin
                  Dec(J);
                  Prev1 := Tpascalword(FileText.Items[J]);
                end;
                functdeclare := (Prev1 <> nil) and (Prev1.ReservedType in [rtEquals, rtColon]);
              end else
                functdeclare := False;
              NoBlankLine := False;
              if not functdeclare then
              begin
                k := 0;
                repeat
                  Inc(k);
                  next := GetWord(I + k);
                  if (next <> nil) and (next.ReservedType = rtLeftBr) then
                    repeat
                      Inc(k);
                      next := GetWord(I + k);
                    until (next = nil) or (next.ReservedType = rtRightBr);
                until (next = nil) or (next.ReservedType = rtSemiColon);
                if next <> nil then
                begin
                  repeat
                    Inc(k);
                    next := GetWord(I + k);
                  until (next = nil) or not (next.ReservedType in
                    [rtLineFeed,
                    rtComment]);
                  if (next <> nil) and (next.ReservedType = rtForward) then
                    NoBlankLine := True;
                end;
              end;
              if not (functdeclare or interfacePart or (GetStackTop = rtClass)) then
              begin
                if not HasType(rtProcedure) then
                begin
                  if (nIndent > 0) then
                  begin
                    nIndent := 0;
                    SetPrevIndent;
                  end;
                  ProcLevel := 0;
                  if BlankProc and not NoBlankLine then
                    CheckBlankProc;
                  (*
                  Removed by Corpsman seems to be Useless.
                  *)
                  //if CommentFunction then
                  //  PutCommentBefore('{ procedure }');
                end else
                begin
                  if BlankSubProc and not NoBlankLine then
                    CheckBlankProc;
                  Inc(ProcLevel);
                  if nIndent = 0 then
                  begin
                    SetPrevIndent;
                    Inc(nIndent);
                  end;
                end;
                Push(rtProcedure, nIndent, 0);
              end else
              begin
                if (not functdeclare) and (not (GetStackTop = rtClass)) then
                begin
                  nIndent := 0;
                  SetPrevIndent;
                end;
                Push(rtProcDeclare, nIndent, 0);
              end;
            end;
          rtInterface:
            begin
              if (prev.ReservedType = rtEquals) then
              begin
                {declaration of a OLE object
                   IClass = interface
                     [' dfgsgdf']}
                Push(rtClassDecl, nIndent, 1);
              end else
              begin
                interfacePart := True;
                DecPrevIndent;
              end;
              WrapIndent := False;
            end;
          rtImplementation:
            begin
              stackptr := -1;
              nIndent := 0;
              interfacePart := False;
              WrapIndent := False;
              nIndent := 0;
              SetPrevIndent;
            end;
          rtBegin, rtTry:
            begin
              if GetStackTop in [rtVar, rtType] then
                Pop;
              if GetStackTop in [rtProcedure, rtProgram] then
                Pop;
              if stackptr = -1 then
                nIndent := 0;
              if NoBeginTryIndent then
                Dec(nIndent);
              case FeedRoundBegin of
                Hanging:
                  begin
                    if (GetStackTop in [rtDo, rtThen, rtIfElse, rtElse, rtColon]) and (prev <> nil) and (GetWord(I - 1) = PrevLine) then
                    begin
                      FileText.AtFree(I - 1);
                      Dec(I);
                      CheckSlashComment;
                    end;
                    MakeLineFeed(I, 1);
                  end;
                NewLine:
                  begin
                    if (prev <> nil) and (GetWord(I - 1).ReservedType <> rtLineFeed) then
                    begin
                      PrevLine := MakeLineFeed(I - 1, 0);
                      prev := PrevLine;
                    end;
                    MakeLineFeed(I, 1);
                  end;
              end;
              Push(rtype, nIndent, 1);
              if prev = PrevLine then
              begin
                SetPrevIndent;
                DecPrevIndent;
              end;
              WrapIndent := False;
            end;
          rtEquals:
            if AlignVar and (GetStackTop = rtVar) then
              PascalWord := AlignExpression(I, AlignVarPos);
          rtVar, rtType:
            if not (GetStackTop in [rtLeftBr, rtLeftHook]) then
            begin
              WrapIndent := False;
              if nIndent < 1 then
                nIndent := 1;
              if (GetStackTop in [rtVar, rtType]) then
                Pop;
              Push(rtype, nIndent, 0);
              if (not ((prev <> nil) and (prev.ReservedType = rtEquals))) then
              begin
                DecPrevIndent;
                if FeedAfterVar then
                  MakeLineFeed(I, 1);
              end;
            end;
          rtCase:
            if not (GetStackTop in [rtRecord, rtLeftBr]) then
              Push(rtype, nIndent, 0)
            else
            begin
              WrapIndent := False;
              Push(rtRecCase, nIndent, 1);
            end;
          rtDo:
            if GetStackTop in [rtWhile, rtOn] then
            begin
              lastPop := GetStackTop;
              Push(rtype, nIndent, 1);
              WrapIndent := False;
              if NoFeedBeforeThen and (prev = PrevLine) then
              begin
                FileText.AtFree(I - 1);
                Dec(I);
                CheckSlashComment;
              end;
              if FeedAfterThen then
              begin
                if MakeLineFeed(I, 1) <> PrevLine then
                begin
                  if (lastPop = rtOn) and Settings.ExceptSingle then
                    CheckShortLine;
                end;
              end;
            end;
          rtEnd:
            begin
              WrapIndent := False;
              repeat
                lastPop := Pop;
              until (stackptr < 0) or (lastPop in [rtClass, rtClassDecl,
                rtRecord, rtTry, rtCase, rtBegin, rtAsm]);
              if stackptr = -1 then
                nIndent := 0;
              if FeedBeforeEnd and (prev <> nil) and
                (GetWord(I - 1).ReservedType <> rtLineFeed) then
              begin
                PrevLine := MakeLineFeed(I - 1, 0);
                prev := PrevLine;
              end;
              if (prev = PrevLine) then
                SetPrevIndent;
              if NoBeginTryIndent then
                Inc(nIndent);
            end;
          rtComment:
            begin
              if IndentComments then
                WrapIndent := False;
              if (stackptr = -1) and (nIndent > 0) then
              begin
                nIndent := 0;
                SetPrevIndent;
              end;
              SetSpacing(GetWord(I + 1), PascalWord, I + 1);
              if ((PrevLine <> nil) and (PrevLine = prev)) then
              begin
                if not IndentComments or (PascalWord.WordType in [wtFullOutComment, wtHalfOutComment]) then
                  PrevLine.nSpaces := PrevLine.oldnSpaces
                else
                begin
                  if PrevOldNspaces >= 0 then
                    PrevLine.nSpaces := PrevLine.nSpaces + (PrevLine.oldnSpaces - PrevOldNspaces)
                  else
                    PrevOldNspaces := PrevLine.oldnSpaces;
                end;
              end
              else
                if AlignComments and (PascalWord.WordType = wtFullComment) then
                begin
                  next := GetWord(I + 1);
                  if (next <> nil) and (next.ReservedType = rtLineFeed) then
                    PascalWord := AlignExpression(I, AlignCommentPos);
                end;
            end;
          rtSemiColon:
            if not (GetStackTop in [rtLeftBr, rtLeftHook]) then
            begin
              while (stackptr >= 0) and (GetStackTop in [rtDo, rtWhile, rtProcDeclare, rtThen, rtProgram, rtUses, rtColon, rtClassDecl]) or (GetStackTop = rtIfElse) do
                Pop;
              WrapIndent := False;
              k := 0;
              repeat
                Inc(k);
                next := GetWord(I + k);
              until (next = nil) or (not (next.ReservedType in [{rtComment, }rtLineFeed]));
              if (next <> nil) then
              begin
                if (next.ReservedType = rtAbsolute) or ((GetStackTop in [rtProcedure, rtProcDeclare, rtClass]) and (next.ReservedType in [rtFuncDirective, rtForward]) and (ProcLevel = 0)) then
                  WrapIndent := True
                else
                  if FeedAfterSemiColon and not (next.ReservedType in [rtForward, rtFuncDirective, rtDefault]) then
                    MakeLineFeed(I, 0);
              end;
            end;
          rtCompIf:
            begin
              Inc(StackStackPtr);
              if StackStackPtr <= 2 then
              begin
                StackStack[StackStackPtr].stack := stack;
                StackStack[StackStackPtr].stackptr := stackptr;
                StackStack[StackStackPtr].ProcLevel := ProcLevel;
                StackStack[StackStackPtr].nIndent := nIndent;
              end;
            end;
          rtCompElse:
            begin
              if (StackStackPtr >= 0) and (StackStackPtr <= 2) then
              begin
                stack := StackStack[StackStackPtr].stack;
                stackptr := StackStack[StackStackPtr].stackptr;
                ProcLevel := StackStack[StackStackPtr].ProcLevel;
                nIndent := StackStack[StackStackPtr].nIndent;
              end;
            end;
          rtCompEndif:
            begin
              if StackStackPtr >= 0 then
                Dec(StackStackPtr);
            end;
        end;
      end;
      SetSpacing(PascalWord, prev, I);
      if not (rtype in [rtLineFeed, rtComment]) then
        PrevOldNspaces := -1;
      if wType = wtCompDirective then
      begin
        WrapIndent := False;
        if not IndentCompDirectives and (prev <> nil) and (prev.ReservedType = rtLineFeed) then
        begin
          NTmp := -nIndent;
          SetPrevIndent;
        end;
        if UpperCompDirectives then
        begin
          P := PascalWord.Expression;
          P2 := P + 1;
          while not (P2^ in [' ', Tab, #0]) do
            Inc(P2);
          while P < P2 do
          begin
            Inc(P);
            P^ := UpCase(P^);
          end;
        end;
      end;
      if not (PascalWord.ReservedType = rtComment) then
        prev := PascalWord;
    end;
  end;
begin {procedure TPascalParser.Parse;}
  LLog.Append('TDelForParser.Parse');
  result := True;
  try
    if Assigned(OnProgress) then
      OnProgress(Self, 33);
    if ChangeIndent then
    begin
      PrevLine := nil;
      PrevPrevLine := nil;
      prev := nil;
      WrapIndent := True;
      interfacePart := False;
      nIndent := 0;
      ProcLevel := 0;
      NTmp := 0;
      PrevOldNspaces := -1;
      stackptr := -1;
      StackStackPtr := -1;
      I := 0;
      with FileText do
        while I < Count do
        begin
          CheckIndent(TPascalWord(Items[I]));
          Inc(I);
          if (I mod 1024 * 4 = 0) and Assigned(OnProgress) then
            OnProgress(Self, Round(33 + I * 33 / FileText.Count));
        end;
    end;
    with FileText do
    begin
      I := Count - 1;
      while (TPascalWord(Items[I]).ReservedType = rtLineFeed) and (I > 0) do
      begin
        AtFree(I);
        Dec(I);
      end;
      if WrapLines or HasAligned then
        CheckWrapping;
    end;
    if Assigned(OnProgress) then
      OnProgress(Self, 66);
  except
    on E: Exception do
    begin
      Clear;
      result := False;
    end;
  end;
end;

procedure TDelForParser.CheckWrapping;
var
  I, J, k: integer;
  K2, lineLen: integer;
  PrevPrevLine: TLineFeed;
  PascalWord: TPascalWord;
  Found: boolean;

  procedure InsertLine(k: integer);
  begin
    PrevPrevLine := PrevLine;
    PrevLine := TLineFeed.Create(PrevPrevLine.oldnSpaces);
    PrevLine.nSpaces := PrevPrevLine.nSpaces;
    PrevLine.wrapped := True;
    TPascalWord(FileText.Items[k]).SetSpace(spBefore, False);
    FileText.Insert(k, PrevLine);
    Found := True;
  end;

begin
  LLog.Append('TDelForParser.CheckWrapping');
  with FileText do
  begin
    lineLen := 0;
    PrevLine := nil;
    J := 0;
    I := 0;
    while I < Count do
    begin
      PascalWord := TPascalWord(Items[I]);
      PascalWord.GetLength(lineLen);
      if WrapLines and (PascalWord is TAlignExpression) and (lineLen > WrapPosition) then
        with TAlignExpression(PascalWord) do
        begin
          k := nSpaces - lineLen - WrapPosition;
          if k < 1 then
            nSpaces := 1
          else
            nSpaces := k;
          lineLen := WrapPosition;
        end;
      if PascalWord.ReservedType = rtLineFeed then
      begin
        PrevLine := TLineFeed(PascalWord);
        if (lineLen > WrapPosition) then
          lineLen := 0;
        J := I;
      end;
      if WrapLines and (lineLen > WrapPosition) and (I > J + 3) then
      begin
        k := I - 1;
        K2 := 0;
        Found := False;
        while (k >= J) and not Found do
        begin
          if (TPascalWord(Items[k]).ReservedType in [rtThen, rtDo]) or ((TPascalWord(Items[k]).ReservedType = rtElse) and (TPascalWord(Items[k + 1]).ReservedType <> rtIf)) then
          begin
            InsertLine(k + 1);
            I := J;
          end;
          if (K2 = 0) and (TPascalWord(Items[k]).Space(spAfter) or
            TPascalWord(Items[k + 1]).Space(spBefore)) then
            K2 := k + 1;
          Dec(k);
        end;
        if not Found and (K2 <> 0) and (K2 > J) then
        begin
          InsertLine(K2);
          I := J;
        end;
        lineLen := 0;
      end;
      Inc(I);
    end;
  end;
end;

function TDelForParser.GetString(Dest: pchar; var I: integer): pchar;
var
  PasWord: TPascalWord;
  P: pchar;
begin
  LLog.Append(Format('TDelForParser.GetString(%s,)', [Dest]));
  GetString := Dest;
  Dest^ := #0;
  P := Dest;
  with FileText do
    if (I < Count) and (I >= 0) then
    begin
      PasWord := TPascalWord(Items[I]);
      repeat
        P := PasWord.GetEString(P);
        Inc(I);
        if I < Count then
          PasWord := TPascalWord(Items[I]);
      until (I >= Count) or (PasWord.ReservedType = rtLineFeed);
    end;
  P := StrEnd(Dest);
  if P - Dest > 1 then
    Dec(P);
  while (P >= Dest) and (P^ in [' ', Tab]) do
  begin
    P^ := #0;
    Dec(P);
  end;
end;

procedure TDelForParser.WriteToFile(AFileName: pchar);
var
  outFile: TextFile;
  I: integer;
  A: array[0..Maxline] of char;
begin
  LLog.Append(Format('TDelForParser.WriteToFile(%s)', [AFileName]));
  Assign(outFile, AFileName);
  try
    Rewrite(outFile);
    I := 0;
    with FileText do
      while I < Count do
        Writeln(outFile, GetString(A, I));
  finally
    Close(outFile);
  end;
end;

procedure TDelForParser.SetTextStr(AText: pchar);
var
  P1, P2: pchar;
  Total, Curr, I, j, k: integer;
  e: TExpression;
begin
  LLog.Append(Format('TDelForParser.SetTextStr(%s)', [AText]));
  P1 := AText;
  Total := StrLen(AText);
  Curr := 0;
  P2 := StrScan(P1, New_Line);
  k := 0;
  while P2 <> nil do
  begin
    Inc(k);
    I := P2 - P1;
    P2^ := #0;
    Add(P1);
    P2^ := New_Line;
    p1 := p2 + CRLF_LEN;
    P2 := StrScan(P1, New_Line);
    Curr := Curr + I + CRLF_LEN;
    if (k mod 50 = 0) and Assigned(OnProgress) then
      OnProgress(Self, (Curr * 34) div Total);
  end;
  Add(P1);

  (*
   * Hack Eingefügt by Corpsman, der das Schlüsselwort "For" umbiegt
   * zu einem Reserved, wenn es nach "Helper" kommt -> Damit die Einrückung stimmt
   * Die Schlüsselworte "Type" und "Record" for "Helper" werden zu einer "Class" Umgebogen -> Damit die Einrückung stimmt
   *)
  for i := 0 to FileText.Count - 1 do
  begin
    if (TPascalWord(FileText[i]) is TExpression) and (TExpression(FileText[i]).FReservedType = rtHelper) then
    begin
      // for -> rtReserved
      for j := i + 1 to FileText.Count - 1 do
      begin
        if (TPascalWord(FileText[j]) is TExpression) and (lowercase(TExpression(FileText[j]).FExpression) = 'for') then
        begin
          e := TExpression(FileText[j]);
          e.FReservedType := rtReserved;
          // Umschreiben, das das For nicht wie eine While verwendet wird.
          break;
        end else
          break;
      end;
      // "Type", "Record" -> rtClass
      for j := i - 1 downto 0 do
      begin
        if (TPascalWord(FileText[j]) is TExpression) and ((lowercase(TExpression(FileText[j]).FExpression) = 'type') or (lowercase(TExpression(FileText[j]).FExpression) = 'record')) then
        begin
          e := TExpression(FileText[j]);
          e.FReservedType := rtClass;
          // Umschreiben, das das For nicht wie eine While verwendet wird.
          break;
        end else
          break;
      end;
    end;
  end;
end;

function TDelForParser.GetTextStr: pchar;
const
  Increment = $FFFF;
var
  CurSize, CurCap: integer;
  Buff: array[0..Maxline] of char;
  P, P2, Pres: pchar;
  I: integer;
begin
  LLog.Append('TDelForParser.GetTextStr');
  CurCap := Increment;
  StrDispose(FCurrentText);
  Pres := StrAlloc(CurCap + 10);
  P := Pres;
  P^ := #0;
  I := 0;
  CurSize := 0;
  with FileText do
    while I < Count do
    begin
      GetString(Buff, I);
      CurSize := CurSize + integer(StrLen(Buff)) + 2;
      while CurSize > CurCap do
      begin
        Inc(CurCap, Increment);
        P2 := Pres;
        Pres := StrAlloc(CurCap + 10);
        P := strECopy(Pres, P2);
        StrDispose(P2);
      end;
      P := strECopy(P, Buff);
      P := strECopy(P, CRLF);
      if (I mod 50 = 0) and Assigned(OnProgress) then
        OnProgress(Self, 66 + I * 34 div Count);
    end;
  result := Pres;
  FCurrentText := Pres;
end;

destructor TDelForParser.Destroy;
begin
  LLog.Append('TDelForParser.Destroy');
  inherited Destroy;
  StrDispose(FCurrentText);
  CapNames.free;
  FileText.Free;
end;

constructor TPascalWord.Create;
begin
  inherited Create;
end;

function TPascalWord.GetExpression: pchar;
begin
  GetExpression := nil;
end;

procedure TPascalWord.GetLength(var Length: integer);
begin
end;

function TPascalWord.WordType: TWordType;
begin
  WordType := wtLineFeed;
end;

function TPascalWord.Space(SpaceBefore: TSpaceBefore): boolean;
begin
  Space := False;
end;

procedure TPascalWord.SetExpression(AExpression: pchar);
begin
end;

procedure TPascalWord.SetCase(ACase: TCase);
begin
end;

function TPascalWord.GetCase: TCase;
begin
  result := rfUnchanged;
end;

procedure TPascalWord.SetSpace(SpaceBefore: TSpaceBefore; State: boolean);
begin
end;

function TPascalWord.ReservedType: TReservedType;
begin
  ReservedType := rtNothing;
end;

procedure TPascalWord.SetReservedType(AReservedType: TReservedType);
begin
end;

constructor TExpression.Create(AType: TWordType; AExpression: pchar);
begin
  FWordType := AType;
  FFormatType := ftNothing;
  FReservedType := rtNothing;
  FExpression := nil;
  SetExpression(AExpression);
  SetSpace(spBoth, False);
  CheckReserved;
end;

procedure TExpression.CheckReserved;
var
  L, H, C, I: integer;
  P: pchar;
  Buf: array[0..Maxline] of char;
begin
  SetReservedType(rtNothing);
  case WordType of
    wtCompDirective:
      begin
        P := StrLower(StrCopy(Buf, Expression));
        if P <> nil then
        begin
          if StrLIComp(P, '{$ifdef', 7) = 0 then
            SetReservedType(rtCompIf)
          else
            if StrLIComp(P, '{$ifndef', 8) = 0 then
              SetReservedType(rtCompIf)
            else
              if StrLIComp(P, '{$else', 6) = 0 then
                SetReservedType(rtCompElse)
              else
                if StrLIComp(P, '{$endif', 7) = 0 then
                  SetReservedType(rtCompEndif);
        end;
      end;
    wtFullComment, wtFullOutComment, wtHalfStarComment,
      wtHalfOutComment, wtHalfComment: SetReservedType(rtComment);
    wtWord:
      begin
        P := StrLower(StrCopy(Buf, Expression));
        if P <> nil then
        begin
          L := 0;
          H := NReservedWords - 1; {fast search method}
          while L <= H do
          begin
            I := (L + H) shr 1;
            C := StrComp(ReservedArray[I].Words, P);
            if C < 0 then
              L := I + 1
            else
            begin
              H := I - 1;
              if C = 0 then
                with ReservedArray[I] do
                begin
                  SetReservedType(ReservedType);
                  Exit;
                end;
            end;
          end;
        end;
      end;
    wtOperator:
      begin
        P := Expression;
        if StrLen(P) = 1 then
        begin
          case P^ of
            ':': SetReservedType(rtColon);
            '.': SetReservedType(rtDot);
            ';': SetReservedType(rtSemiColon);
            ',': SetReservedType(rtComma);
            ')': SetReservedType(rtRightBr);
            '(': SetReservedType(rtLeftBr);
            ']': SetReservedType(rtRightHook);
            '[': SetReservedType(rtLeftHook);
            '-': SetReservedType(rtMinus);
            '+': SetReservedType(rtPlus);
            '=': SetReservedType(rtEquals);
            '/', '*': SetReservedType(rtMathOper);
            '<', '>': SetReservedType(rtLogOper);
          end;
        end
        else
          if StrLen(P) = 2 then
          begin
            if (StrComp(P, '.)') = 0) then
              SetReservedType(rtRightHook)
            else
              if (StrComp(P, '(.') = 0) then
                SetReservedType(rtLeftHook)
              else
                if (StrComp(P, '..') = 0) then
                  SetReservedType(rtDotDot)
                else
                  if (StrComp(P, ':=') = 0) then
                    SetReservedType(rtEqualOper)
                  else
                    if (StrComp(P, '+=') = 0)
                    or (StrComp(P, '-=') = 0)
                    or (StrComp(P, '*=') = 0)
                    or (StrComp(P, '/=') = 0) then
                      SetReservedType(rtEqualOper)
                    else
                      if (P^ = '<')
                      or (P^ = '>') then
                        SetReservedType(rtLogOper);
          end;
      end;
  end;
end;

procedure TExpression.SetExpression(AExpression: pchar);
begin
  StrDispose(FExpression);
  FExpression := StrNew(AExpression);
end;

function TExpression.WordType: TWordType;
begin
  WordType := FWordType;
end;

function TExpression.GetExpression: pchar;
begin
  result := FExpression;
end;

function TExpression.Space(SpaceBefore: TSpaceBefore): boolean;
begin
  case SpaceBefore of
    spBefore: Space := FFormatType and ftSpaceBefore > 0;
    spAfter: Space := FFormatType and ftSpaceAfter > 0;
    spBoth: Space := FFormatType and (ftSpaceAfter or ftSpaceBefore) > 0;
  else
    Space := False;
  end;
end;

function TExpression.ReservedType: TReservedType;
begin
  ReservedType := FReservedType;
end;

destructor TExpression.Destroy;
begin
  SetExpression(nil);
  inherited Destroy;
end;

procedure TExpression.SetSpace(SpaceBefore: TSpaceBefore; State: boolean);
var
  B: byte;
begin
  case SpaceBefore of
    spBefore: B := ftSpaceBefore;
    spAfter: B := ftSpaceAfter;
    spBoth: B := ftSpaceAfter or ftSpaceBefore;
  else
    B := 0;
  end;
  if State then
    FFormatType := FFormatType or B
  else
    FFormatType := FFormatType and not B;
end;

procedure TExpression.SetCase(ACase: TCase);
begin
  FFormatType := FFormatType and not (ftLowerCase + ftUpperCase);
  case ACase of
    rfUpperCase: FFormatType := FFormatType or ftUpperCase;
    rfLowerCase: FFormatType := FFormatType or ftLowerCase;
    rfFirstUp: FFormatType := FFormatType or ftFirstUp;
  end;
end;

function TExpression.GetCase: TCase;
begin
  if (FFormatType and ftUpperCase) > 0 then
    result := rfUpperCase
  else
    if (FFormatType and ftLowerCase) > 0 then
      result := rfLowerCase
    else
      if (FFormatType and ftFirstUp) > 0 then
        result := rfFirstUp
      else
        result := rfUnchanged;
end;

procedure TExpression.SetReservedType(AReservedType: TReservedType);
begin
  FReservedType := AReservedType;
end;

function TExpression.GetEString(Dest: pchar): pchar;
var
  e: pchar;
begin
  if Space(spBefore) then
    result := strECopy(Dest, ' ')
  else
    result := Dest;
  e := Expression;
  if assigned(e) then
    result := strECopy(result, e);
  StrCase(Dest, ExpressionCase);
  if Space(spAfter) then
    result := strECopy(result, ' ');
end;

procedure TExpression.GetLength(var Length: integer);
begin
  if Space(spBefore) then
    Inc(Length);
  if Space(spAfter) then
    Inc(Length);
  if Expression <> nil then
    Inc(Length, StrLen(Expression));
end;

function StrSpace(Dest: pchar; n: integer): pchar;
var
  I: integer;
begin
  result := Dest;
  for I := 0 to n - 1 do
  begin
    Dest^ := ' ';
    Inc(Dest);
  end;
  Dest^ := #0;
end;

function TLineFeed.GetEString(Dest: pchar): pchar;
var
  Len: integer;
begin
  if wrapped and (DelForParser <> nil) then
    Len := nSpaces + DelForParser.SpacePerIndent
  else
    Len := nSpaces;
  if (Len > 0) then
    GetEString := StrEnd(StrSpace(Dest, Len))
  else
    GetEString := Dest;
end;

procedure TLineFeed.GetLength(var Length: integer);
begin
  if nSpaces > 0 then
    Length := nSpaces
  else
    Length := 0;
end;

function TLineFeed.ReservedType: TReservedType;
begin
  ReservedType := rtLineFeed;
end;

constructor TLineFeed.Create(AOldnSpaces: integer);
begin
  inherited Create;
  oldnSpaces := AOldnSpaces;
  wrapped := False;
  nSpaces := AOldnSpaces;
end;

procedure TLineFeed.IncIndent(n: integer);
begin
  if DelForParser <> nil then
    Inc(nSpaces, n * DelForParser.SpacePerIndent);
end;

procedure TLineFeed.SetIndent(n: integer);
begin
  if DelForParser <> nil then
    nSpaces := n * DelForParser.SpacePerIndent;
end;

{ TAlignExpression }

constructor TAlignExpression.Create(Like: TExpression; Pos: byte);
begin
  AlignPos := Pos;
  nSpaces := 0;
  FExpression := StrNew(Like.FExpression);
  FWordType := Like.FWordType;
  FFormatType := Like.FFormatType;
  FReservedType := Like.FReservedType;
end;

procedure TAlignExpression.GetLength(var Length: integer);
begin
  if Length < AlignPos then
  begin
    nSpaces := AlignPos - Length;
    Length := AlignPos;
    SetSpace(spBefore, False);
  end else
    nSpaces := 0;
  inherited GetLength(Length);
end;

function TAlignExpression.GetEString(Dest: pchar): pchar;
begin
  if (nSpaces > 0) then
    result := StrEnd(StrSpace(Dest, nSpaces))
  else
    result := Dest;
  result := inherited GetEString(result);
end;

procedure TDelForParser.SetFillNewWords(AFillNewWords: TFillMode);
begin
  LLog.Append(Format('TDelForParser.SetFillNewWords(%d)', [Ord(AFillNewWords)]));
  FSettings.FillNewWords := AFillNewWords;
end;

procedure TDelForParser.LoadCapFile(ACapFile: pchar);
var
  InFile: TextFile;
  S: array[0..400] of char;
  I: integer;
begin
  LLog.Append(Format('TDelForParser.LoadCapFile(%s)', [ACapFile]));
  if CapNames <> nil then
  begin
    CapNames.FreeAll;
    if (ACapFile <> nil) and FileExists(ACapFile^) then
    begin
      AssignFile(InFile, ACapFile^);
      try
        Reset(InFile);
        while not Eof(InFile) do
        begin
          Readln(InFile, S);
          i := 0;
          if not CapNames.Search(@S, I) then
            CapNames.Insert(I, StrNew(S));
        end;
      finally
        CloseFile(InFile);
      end;
    end
  end;
end;

procedure TDelForParser.SaveCapFile(ACapFile: pchar);
var
  outFile: TextFile;
  I: integer;
begin
  LLog.Append(Format('TDelForParser.SaveCapFile(%s)', [ACapFile]));
  if (ACapFile <> nil) and (ACapFile^ <> #0) then
  begin
    Assign(outFile, ACapFile);
    try
      Rewrite(outFile);
      for I := 0 to CapNames.Count - 1 do
      begin
        Write(outFile, pchar(CapNames.Items[I]));
        Writeln(outFile);
      end;
    finally
      Close(outFile);
    end;
  end;
end;

function TPascalWord.ChangeComment(commchar: char): boolean;
var
  Buff, buff1: array[0..Maxline] of char;
begin
  result := False;
  if ReservedType = rtComment then
  begin
    StrCopy(buff, expression);
    StrCopy(buff1, expression);
    if StrScan(buff, commChar) = nil then
      case commchar of
        '{':
          if StrScan(buff, '}') = nil then
          begin
            StrCopy(buff, '{');
            StrCat(buff, buff1);
            StrCat(buff, '}');
            result := True;
          end;
        '(':
          if strPos(buff, '*)') = nil then
          begin
            StrCopy(buff, '(*');
            StrCat(buff, buff1);
            StrCat(buff, '*)');
            result := True;
          end;
        '/':
          if StrPos(buff, '//') <> @buff[0] then
          begin
            StrCat(StrCopy(buff, '//'), buff1);
            result := True;
          end;
      end;
    expression := buff;
  end;
end;

initialization
  LLog := TLog.Create(CLogName);
  
finalization
  LLog.Free;
  
end.
