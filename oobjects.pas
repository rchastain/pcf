
(*
  Unit: OObjects
  Author: Egbert van Nes
  Copyright (c) 2000  Egbert van Nes
  All rights reserved
  Disclaimer and licence notes: see license.txt
  Modified by Corpsman
*)

unit OObjects;

interface

uses
  Classes;

const
  MaxCollectionSize = MaxInt div (SizeOf(integer) * 2);

type
  TOCollection = class(TList)
  public
    constructor Create(ACapacity: integer);
    procedure AtFree(Index: integer);
    procedure FreeAll;
    procedure DoFree(Item: pointer);
    procedure FreeItem(Item: pointer); virtual;
    destructor Destroy; override;
  end;

  TNoOwnerCollection = class(TOCollection)
  public
    procedure FreeItem(Item: pointer); override;
  end;

  { TSortedCollection }

  TSortedCollection = class(TOCollection)
  public
    Duplicates: boolean;
    constructor Create(ACapacity: integer);
    function Compare(Key1, Key2: pointer): integer; virtual; abstract;
    function IndexOf(Item: pointer): integer; virtual;
    procedure Add(Item: pointer); virtual;
    procedure AddReplace(Item: pointer); virtual;
    function KeyOf(Item: pointer): pointer; virtual;
    function Search(Key: pointer; var Index: integer): boolean; virtual;
  end;

  { TStrCollection }

  TStrCollection = class(TSortedCollection)
  public
    function Compare(Key1, Key2: pointer): integer; override;
    procedure FreeItem(Item: pointer); override;
  end;

implementation

uses
  SysUtils;

constructor TOCollection.Create(ACapacity: integer);
begin
  inherited Create;
  SetCapacity(ACapacity);
end;

destructor TOCollection.Destroy;
begin
  FreeAll;
  inherited Destroy;
end;

procedure TOCollection.AtFree(Index: integer);
var
  Item: pointer;
begin
  Item := Items[Index];
  Delete(Index);
  FreeItem(Item);
end;

procedure TOCollection.FreeAll;
var
  i: integer;
begin
  try
    for i := 0 to Count - 1 do
      FreeItem(Items[i]);
  finally
    Count := 0;
  end;
end;

procedure TOCollection.DoFree(Item: pointer);
begin
  AtFree(IndexOf(Item));
end;

procedure TOCollection.FreeItem(Item: pointer);
begin
  if (Item <> nil) then
    with TObject(Item) as TObject do
      Free;
end;

procedure TNoOwnerCollection.FreeItem(Item: pointer);
begin
end;

{ TSortedCollection }

constructor TSortedCollection.Create(ACapacity: integer);
begin
  inherited Create(ACapacity);
  Duplicates := False;
end;

function TSortedCollection.IndexOf(Item: pointer): integer;
var
  i: integer;
begin
  result := -1;
  if Search(KeyOf(Item), i) then
  begin
    if Duplicates then
      while (i < Count) and (Item <> Items[i]) do
        Inc(i);
    if i < Count then
      result := i;
  end;
end;

procedure TSortedCollection.AddReplace(Item: pointer);
var
  i: integer;
begin
  if Search(KeyOf(Item), i) then
    Delete(i);
  Add(Item);
end;

procedure TSortedCollection.Add(Item: pointer);
var
  i: integer;
begin
  i := 0;
  if not Search(KeyOf(Item), i)
  or Duplicates then
    Insert(i, Item);
end;

function TSortedCollection.KeyOf(Item: pointer): pointer;
begin
  KeyOf := Item;
end;

function TSortedCollection.Search(Key: pointer; var Index: integer): boolean;
var
  L, H, I, C: integer;
begin
  Search := False;
  L := 0;
  H := Count - 1;
  while L <= H do
  begin
    I := (L + H) shr 1;
    C := Compare(KeyOf(Items[I]), Key);
    if C < 0 then
      L := I + 1
    else
    begin
      H := I - 1;
      if C = 0 then
      begin
        Search := True;
        if not Duplicates then
          L := I;
      end;
    end;
  end;
  Index := L;
end;

{ TStrCollection }

function TStrCollection.Compare(Key1, Key2: pointer): integer;
begin
  Compare := StrComp(Key1, Key2);
end;

procedure TStrCollection.FreeItem(Item: pointer);
begin
  StrDispose(Item);
end;

end.
