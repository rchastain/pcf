unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  Menus, StdCtrls, Spin, SynEdit, SynHighlighterPas;

type

  { TForm1 }

  TForm1 = class(TForm)
    BTLoadSettings: TButton;
    BTSaveSettings: TButton;
    BTFormat: TButton;
    CBWrap: TCheckBox;
    CBRemoveDoubleBlank: TCheckBox;
    CBBlankProc: TCheckBox;
    CBBlankSubProc: TCheckBox;
    CBFeedRoundBegin: TComboBox;
    CBFeedEachUnit: TCheckBox;
    CBFeedAfterVar: TCheckBox;
    CBFeedAfterThen: TCheckBox;
    CBExceptSingle: TCheckBox;
    CBFillNewWords: TComboBox;
    CBNoFeedBeforeThen: TCheckBox;
    CBFeedElseIf: TCheckBox;
    CBFeedBeforeEnd: TCheckBox;
    CBFeedAfterSemiColon: TCheckBox;
    CBChangeIndent: TCheckBox;
    CBIndentBegin: TCheckBox;
    CBIndentComments: TCheckBox;
    CBIndentComp: TCheckBox;
    CBReservedCase: TComboBox;
    CBStandDirectCase: TComboBox;
    MMMenu: TMainMenu;
    MIFile: TMenuItem;
    MIQuit: TMenuItem;
    ODLoad: TOpenDialog;
    PCSettings: TPageControl;
    SDSave: TSaveDialog;
    SEWrapPos: TSpinEdit;
    SESpacePerIndent: TSpinEdit;
    Splitter1: TSplitter;
    EDIn: TSynEdit;
    EDOut: TSynEdit;
    SNPasHi: TSynPasSyn;
    STNewWords: TStaticText;
    STStandDirect: TStaticText;
    STReservedCase: TStaticText;
    STFeedRoundBegin: TStaticText;
    TSIndent: TTabSheet;
    TSSpace: TTabSheet;
    TSCase: TTabSheet;
    TSMisc: TTabSheet;
    TSLines: TTabSheet;
    procedure BTFormatClick(Sender: TObject);
    procedure BTLoadSettingsClick(Sender: TObject);
    procedure BTSaveSettingsClick(Sender: TObject);
    procedure CBChangeIndentChange(Sender: TObject);
    procedure CBWrapChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MIQuitClick(Sender: TObject);
  private
    procedure SettingsToComponents;
    procedure ComponentsToSettings;
  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

uses
  TypInfo, DelForTypes, DelForEngine, Settings;

var
  Settings1: TSettings;

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
const
  CSourceCodeExample = '../example.pas';
  CDefaultCodeExample = 'program Example; begin end.';
var
  LFeedBegin: TFeedBegin;
  LFillMode: TFillMode;
  LCase: TCase;
begin
  for LFeedBegin in TFeedBegin do
    CBFeedRoundBegin.Items.Append(GetEnumName(TypeInfo(TFeedBegin), Ord(LFeedBegin)));
  for LFillMode in TFillMode do
    CBFillNewWords.Items.Append(GetEnumName(TypeInfo(TFillMode), Ord(LFillMode)));
  for LCase in TCase do
  begin
    CBReservedCase.Items.Append(GetEnumName(TypeInfo(TCase), Ord(LCase)));
    CBStandDirectCase.Items.Append(GetEnumName(TypeInfo(TCase), Ord(LCase)));
  end;

  Settings1 := DefaultSettings;
  SettingsToComponents;

  if Assigned(DelForParser) then
    DelForParser.Free;
  DelForParser := TDelForParser.Create;

  if FileExists(CSourceCodeExample) then
    EDIn.Lines.LoadFromFile(CSourceCodeExample)
  else
    EDIn.Lines.Text := CDefaultCodeExample;
end;

procedure TForm1.FormActivate(Sender: TObject);
var
  LMenuHeight: integer;
begin
  LMenuHeight := Self.Height - Self.ClientHeight;
  Self.Height := Splitter1.Height + LMenuHeight + 2 * 8;
  Splitter1.Anchors := Splitter1.Anchors + [akBottom];
  Self.Left := 8;
  Self.Top := 8;
  Self.Height := 7 * Screen.Height div 8;
  Self.Width := Screen.Width - 2 * 8;
  // ----
  BTFormat.Click;
  // ----
end;

procedure TForm1.BTFormatClick(Sender: TObject);
var
  LText: pchar;
begin
  ComponentsToSettings;

  DelForParser.Clear;
  DelForParser.Settings := Settings1;

  LText := @EDIn.Lines.Text[1];
  DelForParser.Text := LText;

  if DelForParser.Parse then
  begin
    LText := DelForParser.Text;
    EDOut.Lines.Text := LText;
    EDOut.TopLine := EDIn.TopLine;
  end else
    ShowMessage('Format operation failed.');
end;

procedure TForm1.BTLoadSettingsClick(Sender: TObject);
begin
  if ODLoad.Execute then
    if ExtractFileExt(ODLoad.FileName) = '.ini' then
      IniLoadSettings(@Settings1, ODLoad.FileName)
    else
      LoadSettings(@Settings1, ODLoad.FileName);
end;

procedure TForm1.BTSaveSettingsClick(Sender: TObject);
begin
  if SDSave.Execute then
    if ExtractFileExt(SDSave.FileName) = '.ini' then
      IniSaveSettings(@Settings1, SDSave.FileName)
    else
      SaveSettings(@Settings1, SDSave.FileName);
end;

procedure TForm1.CBChangeIndentChange(Sender: TObject);
begin
  SESpacePerIndent.Enabled := TCheckBox(Sender).Checked;
end;

procedure TForm1.CBWrapChange(Sender: TObject);
begin
  SEWrapPos.Enabled := TCheckBox(Sender).Checked;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  DelForParser.Free;
  DelForParser := nil;
end;

procedure TForm1.MIQuitClick(Sender: TObject);
begin
  Close;
end;

procedure TForm1.SettingsToComponents;
begin
  with Settings1 do
  begin
    (* Lines *)
    CBWrap.Checked := WrapLines;
    SEWrapPos.Value := WrapPosition;
    CBRemoveDoubleBlank.Checked := RemoveDoubleBlank;
    CBBlankProc.Checked := BlankProc;
    CBBlankSubProc.Checked := BlankSubProc;
    CBFeedRoundBegin.ItemIndex := Ord(FeedRoundBegin);
    CBFeedEachUnit.Checked := FeedEachUnit;
    CBFeedAfterVar.Checked := FeedAfterVar;
    CBFeedAfterThen.Checked := FeedAfterThen;
    CBNoFeedBeforeThen.Checked := NoFeedBeforeThen;
    CBFeedElseIf.Checked := FeedElseIf;
    CBFeedBeforeEnd.Checked := FeedBeforeEnd;
    CBFeedAfterSemicolon.Checked := FeedAfterSemicolon;
    (* Indentation *)
    CBChangeIndent.Checked := ChangeIndent;
    SESpacePerIndent.Value := SpacePerIndent;
    CBIndentBegin.Checked := IndentBegin;
    CBIndentComments.Checked := IndentComments;
    CBIndentComp.Checked := IndentCompDirectives;

    (* Case *)
    CBReservedCase.ItemIndex := Ord(ReservedCase);
    CBStandDirectCase.ItemIndex := Ord(StandDirectivesCase);

    (* Misc *)
    CBFillNewWords.ItemIndex := Ord(FillNewWords);
  end;
end;

procedure TForm1.ComponentsToSettings;
begin
  with Settings1 do
  begin
    (* Lines *)
    WrapLines := CBWrap.Checked;
    WrapPosition := SEWrapPos.Value;
    RemoveDoubleBlank := CBRemoveDoubleBlank.Checked;
    BlankProc := CBBlankProc.Checked;
    BlankSubProc := CBBlankSubProc.Checked;
    FeedRoundBegin := TFeedBegin(CBFeedRoundBegin.ItemIndex);
    FeedEachUnit := CBFeedEachUnit.Checked;
    FeedAfterVar := CBFeedAfterVar.Checked;
    FeedAfterThen := CBFeedAfterThen.Checked;
    NoFeedBeforeThen := CBNoFeedBeforeThen.Checked;
    FeedElseIf := CBFeedElseIf.Checked;
    FeedBeforeEnd := CBFeedBeforeEnd.Checked;
    FeedAfterSemicolon := CBFeedAfterSemicolon.Checked;
    (* Indentation *)
    ChangeIndent := CBChangeIndent.Checked;
    SpacePerIndent := SESpacePerIndent.Value;
    IndentBegin := CBIndentBegin.Checked;
    IndentComments := CBIndentComments.Checked;
    IndentCompDirectives := CBIndentComp.Checked;

    (* Case *)
    ReservedCase := TCase(CBReservedCase.ItemIndex);
    StandDirectivesCase := TCase(CBStandDirectCase.ItemIndex);

    (* Misc *)
    FillNewWords := TFillMode(CBFillNewWords.ItemIndex);
  end;
end;

end.

