
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
 sysutils,typinfo,msetypes, mseglob, mseguiglob, mseguiintf, mseapplication,
 msestat, msemenus,msegui,msegraphics, msegraphutils, mseevent, mseclasses,
 msewidgets, mseforms,msedragglob,msescrollbar, msetabs,msegraphedits,
 mseificomp,mseificompglob,mseifiglob,mseact,msedataedits,msedropdownlist,
 mseedit,msestatfile,msestream,msegrids,msegridsglob,msewidgetgrid,msesplitter,
 msesyntaxpainter,mseeditglob,mserichstring,msesyntaxedit,msetextedit,
 msesimplewidgets,msebitmap,msedatanodes,msefiledialog,mselistbrowser,msesys,
 msesysintf;

type
  tmainfo = class(tmainform)
    widget1: ttabwidget;
    page1: ttabpage;
    page2: ttabpage;
    page3: ttabpage;
    page4: ttabpage;
    page5: ttabpage;
   editwrap: tbooleanedit;
   editwrappos: trealspinedit;
   editremovedoubleblank: tbooleanedit;
   editblankproc: tbooleanedit;
   editblanksubproc: tbooleanedit;
   editfeedroundbegin: tenumtypeedit;
   editfeedeachunit: tbooleanedit;
   editfeedaftervar: tbooleanedit;
   editfeedafterthen: tbooleanedit;
   editnofeedbeforethen: tbooleanedit;
   editfeedelseif: tbooleanedit;
   editfeedbeforeend: tbooleanedit;
   editfeedsemicolon: tbooleanedit;
   editchangeindent: tbooleanedit;
   editspaceperindent: trealspinedit;
   editindentbegin: tbooleanedit;
   editindentcomments: tbooleanedit;
   editindentdirectives: tbooleanedit;
   editnoindentelseif: tbooleanedit;
   editindenttryelse: tbooleanedit;
   editindentcaseelse: tbooleanedit;
   editindenttry: tbooleanedit;
   editspaceoperators: tenumtypeedit;
   editspaceequaloper: tenumtypeedit;
   editspacecolon: tenumtypeedit;
   editspacesemicolon: tenumtypeedit;
   editspacecomma: tenumtypeedit;
   editspaceleftbr: tenumtypeedit;
   editspacerightbr: tenumtypeedit;
   editspacelefthook: tenumtypeedit;
   editspacerighthook: tenumtypeedit;
   editcasereserved: tenumtypeedit;
   editcasestandarddirectives: tenumtypeedit;
   editupcompilerdirectives: tbooleanedit;
   editupnumbers: tbooleanedit;
   editaligncomments: tbooleanedit;
   editalignvar: tbooleanedit;
   editaligncommentpos: trealspinedit;
   editalignvarpos: trealspinedit;
   editexceptsingle: tbooleanedit;
   editstartcommentout: tstringedit;
   editendcommentout: tstringedit;
   editfillnewwords: tenumtypeedit;
   editsupportcoperators: tbooleanedit;
   grid1: twidgetgrid;
   grid2: twidgetgrid;
   tsplitter1: tsplitter;
   editpascal1: tsyntaxedit;
   editpascal2: tsyntaxedit;
   painter1: tsyntaxpainter;
   buttonformat: tbutton;
   buttonloadsettings: tbutton;
   buttonsavesettings: tbutton;
   buttonloadsource: tbutton;
   buttonsavesource: tbutton;
   editautomaticformat: tbooleanedit;
   dialogopenfile: tfiledialog;
   procedure tfeededitinit(const sender: tenumtypeedit);
   procedure editwrapchange(const sender: TObject);
   procedure tspaceeditinit(const sender: tenumtypeedit);
   procedure tcaseeditinit(const sender: tenumtypeedit);
   procedure tfillmodeeditinit(const sender: tenumtypeedit);
   procedure formcreate(const sender: TObject);
   procedure buttonformatexecute(const sender: TObject);
   procedure formdestroy(const sender: TObject);
   procedure editautomaticchange(const sender: TObject);
   procedure optionchange(const sender: TObject);
   procedure buttonloadsource_onexecute(const sender: TObject);
  private
    procedure SettingsToComponents;
    procedure ComponentsToSettings;
    procedure TryFormat;
  end;

var
  mainfo: tmainfo;
  
implementation

uses
  main_mfm,
  DelForTypes, DelForEngine, Settings;

var
  pasforsettings: TSettings;

procedure tmainfo.tfeededitinit(const sender: tenumtypeedit);
begin
  sender.typeinfopo := PTypeInfo(TypeInfo(TFeedBegin));
end;

procedure tmainfo.editwrapchange(const sender: TObject);
begin
  editwrappos.enabled := tbooleanedit(sender).value;
  optionchange(sender);
end;

procedure tmainfo.tspaceeditinit(const sender: tenumtypeedit);
begin
  sender.typeinfopo := PTypeInfo(TypeInfo(TSpaceBefore));
end;

procedure tmainfo.tcaseeditinit(const sender: tenumtypeedit);
begin
  sender.typeinfopo := PTypeInfo(TypeInfo(TCase));
end;

procedure tmainfo.tfillmodeeditinit(const sender: tenumtypeedit);
begin
  sender.typeinfopo := PTypeInfo(TypeInfo(TFillMode));
end;

procedure tmainfo.formcreate(const sender: TObject);
var
  i: integer;
  s: ttextstream;
  lsourceexample: msestring;
begin
  s := ttextstream.create('pascal.sdef');
  i := painter1.readdeffile(s);
  if i >= 0 then
  begin
    editpascal1.setsyntaxdef(i);
    editpascal2.setsyntaxdef(i);
  end;
  s.free;
  // ----
  lsourceexample := ExtractFilePath(sys_getapplicationpath) + 'example.pas';
  if FileExists(lsourceexample) then
  begin
    editpascal1.loadfromfile(lsourceexample);
    caption := lsourceexample;
  end else
    editpascal1.settext('program Example;begin end.');
  // ----
  pasforsettings := DefaultSettings;
  SettingsToComponents;
  // ----
  if Assigned(DelForParser) then
    DelForParser.Free;
  DelForParser := TDelForParser.Create;
end;

procedure tmainfo.buttonformatexecute(const sender: TObject);
begin
  TryFormat;  
end;

procedure tmainfo.formdestroy(const sender: TObject);
begin
  DelForParser.Free;
  DelForParser := nil;
end;

procedure tmainfo.SettingsToComponents;
begin
  with pasforsettings do
  begin
    (* Lines *)
    editwrap.value := WrapLines;
    editwrappos.value := WrapPosition;
    editremovedoubleblank.value := RemoveDoubleBlank;
    editblankproc.value := BlankProc;
    editblanksubproc.value := BlankSubProc;{
    CBFeedRoundBegin.ItemIndex := Ord(FeedRoundBegin);
    CBFeedEachUnit.Checked := FeedEachUnit;
    CBFeedAfterVar.Checked := FeedAfterVar;
    CBFeedAfterThen.Checked := FeedAfterThen;
    CBNoFeedBeforeThen.Checked := NoFeedBeforeThen;
    CBFeedElseIf.Checked := FeedElseIf;
    CBFeedBeforeEnd.Checked := FeedBeforeEnd;
    CBFeedAfterSemicolon.Checked := FeedAfterSemicolon;
    (* Indentation *)
    CBChangeIndent.Checked := ChangeIndent;
    SESpacePerIndent.Value := SpacePerIndent;
    CBIndentBegin.Checked := IndentBegin;
    CBIndentComments.Checked := IndentComments;
    CBIndentComp.Checked := IndentCompDirectives;

    (* Case *)
    CBReservedCase.ItemIndex := Ord(ReservedCase);
    CBStandDirectCase.ItemIndex := Ord(StandDirectivesCase);

    (* Misc *)
    CBFillNewWords.ItemIndex := Ord(FillNewWords);}
  end;
end;

procedure tmainfo.ComponentsToSettings;
begin
  with pasforsettings do
  begin
    (* Lines *)
    WrapLines := editwrap.value;
    WrapPosition := editwrappos.intvalue;
    RemoveDoubleBlank := editremovedoubleblank.value;
    BlankProc := editblankproc.value;
    BlankSubProc := editblanksubproc.value;{
    FeedRoundBegin := TFeedBegin(CBFeedRoundBegin.ItemIndex);
    FeedEachUnit := CBFeedEachUnit.Checked;
    FeedAfterVar := CBFeedAfterVar.Checked;
    FeedAfterThen := CBFeedAfterThen.Checked;
    NoFeedBeforeThen := CBNoFeedBeforeThen.Checked;
    FeedElseIf := CBFeedElseIf.Checked;
    FeedBeforeEnd := CBFeedBeforeEnd.Checked;
    FeedAfterSemicolon := CBFeedAfterSemicolon.Checked;
    (* Indentation *)
    ChangeIndent := CBChangeIndent.Checked;
    SpacePerIndent := SESpacePerIndent.Value;
    IndentBegin := CBIndentBegin.Checked;
    IndentComments := CBIndentComments.Checked;
    IndentCompDirectives := CBIndentComp.Checked;

    (* Case *)
    ReservedCase := TCase(CBReservedCase.ItemIndex);
    StandDirectivesCase := TCase(CBStandDirectCase.ItemIndex);

    (* Misc *)
    FillNewWords := TFillMode(CBFillNewWords.ItemIndex);}
  end;
end;

procedure tmainfo.TryFormat;
var
  LText: pchar;
  LStr1: msestring;
  LStr2: string;
begin
  DelForParser.Clear;
  DelForParser.Settings := pasforsettings;
  
  LStr1 := editpascal1.gettext;
  LStr2 := string(LStr1);  
  LText := @LStr2[1];
  
  DelForParser.Text := LText;  
  
  if DelForParser.Parse then
  begin
   LText := DelForParser.Text;  
   editpascal2.settext(LText);
  end else
    ShowMessage('Format operation failed.');
end;

procedure tmainfo.editautomaticchange(const sender: TObject);
begin
 {buttonformat.enabled := not tbooleanedit(sender).value;}
end;

procedure tmainfo.optionchange(const sender: TObject);
begin
  ComponentsToSettings;
  if editautomaticformat.value then
    TryFormat;
end;

procedure tmainfo.buttonloadsource_onexecute(const sender: TObject);
begin
  dialogopenfile.controller.filter := '"*.lpr" "*.pas"';
  (*
  dialogopenfile.controller.filterlist.clear;
  dialogopenfile.controller.filterlist.add('Pascal program', '*.pas');
  *)
  if dialogopenfile.execute = MR_OK then
  begin
    editpascal1.loadfromfile(dialogopenfile.controller.filename);
    caption := dialogopenfile.controller.filename;
  end;
end;

end.
