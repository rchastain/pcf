unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  BGRABitmap, BGRABitmapTypes;

type

  { TForm1 }

  TForm1 = class(TForm)
    BTSave: TButton;
    CBBackground: TCheckBox;
    procedure BTSaveClick(Sender: TObject);
    procedure CBBackgroundChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormPaint(Sender: TObject);
  private
    FBitmap: TBGRABitmap;
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

const
  CImageWidth = 256;
  CImageHeight = CImageWidth;
  CLineWidth = 18;
  CText = 'PCF';
  CFileName = 'icon.png';
  CFontName = {'Source Code Pro'}{'Cantarell'}'Mono';

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  FBitmap := TBGRABitmap.Create(CImageWidth, CImageHeight, BGRAPixelTransparent);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FBitmap.Free;
end;

procedure TForm1.BTSaveClick(Sender: TObject);
begin
  FBitmap.SaveToFile(CFileName);
  FBitmap.SaveToFile(FormatDateTime('YYMMDDHHMMSS".png"', Now));
end;

procedure TForm1.CBBackgroundChange(Sender: TObject);
begin
  Invalidate;
end;

procedure TForm1.FormPaint(Sender: TObject);
var
  sz: TSize;
  y, i: integer;
  blanc, gris, bleu, bleuclair: TBGRAPixel;
begin
  blanc     := BGRA($FE, $FB, $D8, 255);
  gris      := BGRA($61, $86, $85, 255);
  bleu      := BGRA($36, $48, $6B, 255);
  bleuclair := BGRA($40, $40, $A1, 255);
  
  FBitmap.Fill(BGRAPixelTransparent);
  
  if CBBackground.Checked then
  begin
    FBitmap.FillRectAntialias(0, 0, CImageWidth, CImageHeight div 4, bleu);
    FBitmap.FillRectAntialias(0, CImageHeight div 4, CImageWidth, 3 * CImageHeight div 4, blanc);
    FBitmap.FillRectAntialias(0, 3 * CImageHeight div 4, CImageWidth, CImageHeight, gris);
  end;

  FBitmap.FontName := CFontName;
  FBitmap.FontAntialias := True;
  FBitmap.FontHeight := CImageHeight div 2;
  FBitmap.FontStyle := [fsBold];
  
  sz := FBitmap.TextSize(CText);
  FBitmap.TextOut((CImageWidth - sz.cx) div 2, (CImageHeight - sz.cy) div 2, CText, bleuclair);
  
  FBitmap.Draw(Canvas, 8, 8, False);
end;

end.
