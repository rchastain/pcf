
program cfg2ini;

uses
  SysUtils, DelForTypes, Settings;

var
  LSettings: PSettings;
  
begin
  if (ParamCount = 1)
  and FileExists(ParamStr(1)) then
  begin
    New(LSettings);
    LoadSettings(LSettings, ParamStr(1));
    IniSaveSettings(LSettings, ChangeFileExt(ParamStr(1), '.ini'));
    Dispose(LSettings);
  end;
end.
