(* Source code example for the Pascal Source Code Formatter. *)
program Example;
{$ifdef windows}
{$apptype console}
{$endif}
uses SysUtils,Classes,Math;// Hello



PROCEDURE MyProcedure;
procedure MySubProcedure;begin end;
Begin
End;
{(*}
const
  h =  1;
  i = 11;
{*)}
var a, b, c, d, e, f: byte; g: array[0..1]of Byte;
h: char; // char
  i: Char; // Char
    j: CHAR; // CHAR
begin if (a = 0) or ( b=$AA ) then WriteLn('Hello');if g[0]=2+2 then MyProcedure;end.
