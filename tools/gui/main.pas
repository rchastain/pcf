
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
 msetypes, mseglob, mseguiglob, mseguiintf, mseapplication, msestat, msemenus,
 msegui, msegraphics, msegraphutils, mseevent, mseclasses, msewidgets, mseforms,
 msebitmap, msedataedits, msedatanodes, mseedit, msefiledialog, msegrids,
 mseificomp, mseificompglob, mseifiglob, mselistbrowser, msestrings, msesys,
 msesimplewidgets, msepipestream, mseprocess, mseact, msedropdownlist,
 msestatfile, msestream, sysutils,msedragglob,msegridsglob;

type
  tmainfo = class(tmainform)
    edexecutable: tfilenameedit;
    btrun: tbutton;
    grid: tstringgrid;
    proc: tmseprocess;
    edinput: tfilenameedit;
    edoutput: tfilenameedit;
    edconfig: tfilenameedit;
    btquit: tbutton;
    procedure runexe(const sender: TObject);
    procedure inputavailexe(const sender: tpipereader);
    procedure procfinishedexe(const sender: TObject);
   procedure quitexe(const sender: TObject);
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm;

procedure log(const astr: string);
var
  lname: tfilename;
  lfile: text;
begin
  lname := changefileext({$I %FILE%}, '.log');
  assign(lfile, lname);
  if fileexists(lname) then
    append(lfile)
  else
    rewrite(lfile);
  writeln(lfile, astr);
  close(lfile);
end;

procedure tmainfo.runexe(const sender: TObject);
begin
  proc.active := false;
  grid.clear;
  proc.commandline := format('%s %s -o %s -c %s', [edexecutable.value, edinput.value, edoutput.value, edconfig.value]);
  log(concat('proc.commandline = ', proc.commandline));
  proc.active := true;
end;

procedure tmainfo.inputavailexe(const sender: tpipereader);
begin
  grid[0].readpipe(sender);
  grid.showlastrow;
end;

procedure tmainfo.procfinishedexe(const sender: TObject);
begin
  grid.appendrow('*** process finished ***');
end;

procedure tmainfo.quitexe(const sender: TObject);
begin
  close;
end;

end.
