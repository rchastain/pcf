
unit Log;

interface

uses
  SysUtils;
  
type
  TLog = class
    private
      FFile: text;
    public
      constructor Create(const AName: TFileName); overload;
      destructor Destroy; override;
      procedure Append(const ALine: string; const AFlush: boolean = FALSE);
  end;

implementation

constructor TLog.Create(const AName: TFileName);
begin
  AssignFile(FFile, AName);
  Rewrite(FFile);
end;

destructor TLog.Destroy;
begin
  CloseFile(FFile);
end;

procedure TLog.Append(const ALine: string; const AFlush: boolean);
begin
  WriteLn(FFile, ALine);
  if AFlush then
    Flush(FFile);
end;

end.
