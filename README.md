
# Pascal Code Formatter

## Description

Pascal source code formatter written in Pascal by Egbert van Nes.

## History

The name of the original project is *DelForExp* (Delphi Formatter Expert).

*DelForExp* has been [adapted for Lazarus](http://corpsman.de/index.php?doc=projekte/delforlaz) by [Uwe Schächterle](https://corpsman.de/).
 
Finally, the Lazarus Formatter Expert has been changed to a stand-alone command-line tool by me (Roland Chastain), under the name *PCF*.

## Usage

```
  pcf -h
  pcf input.pas
  pcf input.pas -o output.pas
  pcf input.pas -o output.pas -c settings.cfg
```

Long options are also accepted.

```
  pcf --help
  pcf input.pas --out=output.pas --conf=settings.cfg
```

* When no output file is specified, the formatted code is written to standard output.
* The input file and the output file can be the same file. The file will be overwritten without asking confirmation.
* When no configuration file is specified, the program searches for a file named *pcf.cfg* or *pcf.ini* (see below).
* Two configuration file formats are supported: binary file or text file. The text file must have a *.ini* extension. Both formats can be created used the Settings Editor.

## Editors

There are two versions on the Settings Editor: one made with Lazarus, the other made with MSEide.

## Credits

The command-line tool uses [UTF8 Tools](https://wiki.freepascal.org/UTF8_Tools) (source included in this repository).

## License

See the file *license.txt*.

This project is published with kind permission of Egbert van Nes.
