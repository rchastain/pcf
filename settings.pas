
unit Settings;

interface

uses
  SysUtils, DelForTypes;

function DefaultSettings(const ABorland: boolean = TRUE): TSettings;
procedure SaveSettings(const ASettings: PSettings; const AFileName: TFileName);
procedure LoadSettings(const ASettings: PSettings; const AFileName: TFileName);
procedure IniSaveSettings(const ASettings: PSettings; const AFileName: TFileName);
procedure IniLoadSettings(const ASettings: PSettings; const AFileName: TFileName);

implementation

uses
  Classes, IniFiles, TypInfo, Log;

const
  CLogName = 'pcf_settings.log';
  
var
  LLog: TLog;
  
function DefaultSettings(const ABorland: boolean): TSettings;
begin
  with result do
  begin
    WrapLines := False or ABorland;
    WrapPosition := 121;
    AlignCommentPos := 40;
    AlignComments := False;
    AlignVarPos := 20;
    AlignVar := False;
    SpaceEqualOper := spBoth;
    SpaceOperators := spBoth;
    SpaceColon := spAfter;
    SpaceComma := spAfter;
    SpaceSemiColon := spAfter;
    SpaceLeftBr := spNone;
    SpaceRightBr := spNone;
    SpaceLeftHook := spNone;
    SpaceRightHook := spNone;
    ReservedCase := rfLowerCase;
    StandDirectivesCase := rfLowerCase;
    ChangeIndent := True;
    IndentBegin := False;
    IndentComments := False or ABorland;
    IndentCompDirectives := False;
    IndentTryElse := False;
    IndentCaseElse := False;
    FeedAfterThen := False or ABorland;
    ExceptSingle := False;
    FeedElseIf := False;
    FeedEachUnit := False;
    NoFeedBeforeThen := False or ABorland;
    NoIndentElseIf := False;
    FeedAfterVar := False or ABorland;
    FeedBeforeEnd := False or ABorland;
    if ABorland then
      FeedRoundBegin := NewLine
    else
      FeedRoundBegin := UnChanged;
    FeedAfterSemiColon := False or ABorland;
    FillNewWords := fmUnchanged;
    IndentTry := False;
    UpperCompDirectives := True;
    UpperNumbers := True;
    SpacePerIndent := 2;
    BlankProc := True;
    RemoveDoubleBlank := False or ABorland;
    BlankSubProc := False;
    StrCopy(StartCommentOut, '{(*}');
    StrCopy(EndCommentOut, '{*)}');
    SupportCOperators := False;
  end;
end;

procedure SaveSettings(const ASettings: PSettings; const AFileName: TFileName);
var
  LStream: TFileStream;
begin
  LLog.Append(Format('SaveSettings(,%s)', [AFileName]));
  LStream := TFileStream.Create(AFileName, fmOpenWrite or fmCreate);
  LStream.Write(ASettings^, SizeOf(TSettings));
  LStream.Free;
end;

procedure LoadSettings(const ASettings: PSettings; const AFileName: TFileName);
var
  LStream: TFileStream;
  LSize: longint;
begin
  LLog.Append(Format('LoadSettings(,%s)', [AFileName]));
  LStream := TFileStream.Create(AFileName, fmOpenRead);
  try
    try
      LSize := LStream.Size;
      LLog.Append(Format('File size: %d bytes', [LSize]));
      if LSize = SizeOf(TSettings) then
        LStream.Read(ASettings^, SizeOf(TSettings))
      else
        LLog.Append('Unexpected file size. Read operation aborted.');
    except
      on E: Exception do
        LLog.Append(Format('Cannot read file: %s', [E.Message]));
    end;
  finally
    LStream.Free;
  end;
end;

const
  CSection = 'settings';

procedure IniSaveSettings(const ASettings: PSettings; const AFileName: TFileName);
begin
  LLog.Append(Format('IniSaveSettings(,%s)', [AFileName]));
  with TIniFile.Create(AFileName) do
  try
    with ASettings^ do
    begin
      //WriteInteger(CSection, 'shortcut', Shortcut);
      WriteString(CSection, 'spaceoperators', GetEnumName(TypeInfo(TSpaceBefore), Ord(SpaceOperators)));
      WriteString(CSection, 'spacecolon', GetEnumName(TypeInfo(TSpaceBefore), Ord(SpaceColon)));
      WriteString(CSection, 'spacesemicolon', GetEnumName(TypeInfo(TSpaceBefore), Ord(SpaceSemiColon)));
      WriteString(CSection, 'spacecomma', GetEnumName(TypeInfo(TSpaceBefore), Ord(SpaceComma)));
      WriteString(CSection, 'spaceleftbr', GetEnumName(TypeInfo(TSpaceBefore), Ord(SpaceLeftBr)));
      WriteString(CSection, 'spacerightbr', GetEnumName(TypeInfo(TSpaceBefore), Ord(SpaceRightBr)));
      WriteString(CSection, 'spacelefthook', GetEnumName(TypeInfo(TSpaceBefore), Ord(SpaceLeftHook)));
      WriteString(CSection, 'spacerighthook', GetEnumName(TypeInfo(TSpaceBefore), Ord(SpaceRightHook)));
      WriteString(CSection, 'spaceequaloper', GetEnumName(TypeInfo(TSpaceBefore), Ord(SpaceEqualOper)));
      WriteString(CSection, 'uppercompdirectives', BoolToStr(UpperCompDirectives, TRUE));
      WriteString(CSection, 'uppernumbers', BoolToStr(UpperNumbers, TRUE));
      WriteString(CSection, 'reservedcase', GetEnumName(TypeInfo(TCase), Ord(ReservedCase)));
      WriteString(CSection, 'standdirectivescase', GetEnumName(TypeInfo(TCase), Ord(StandDirectivesCase)));
      WriteString(CSection, 'changeindent', BoolToStr(ChangeIndent, TRUE));
      WriteString(CSection, 'noindentelseif', BoolToStr(NoIndentElseIf, TRUE));
      WriteString(CSection, 'indentbegin', BoolToStr(IndentBegin, TRUE));
      WriteString(CSection, 'indenttry', BoolToStr(IndentTry, TRUE));
      WriteString(CSection, 'indenttryelse', BoolToStr(IndentTryElse, TRUE));
      WriteString(CSection, 'indentcaseelse', BoolToStr(IndentCaseElse, TRUE));
      WriteString(CSection, 'indentcomments', BoolToStr(IndentComments, TRUE));
      WriteString(CSection, 'indentcompdirectives', BoolToStr(IndentCompDirectives, TRUE));
      WriteString(CSection, 'blankproc', BoolToStr(BlankProc, TRUE));
      WriteString(CSection, 'blanksubproc', BoolToStr(BlankSubProc, TRUE));
      WriteString(CSection, 'removedoubleblank', BoolToStr(RemoveDoubleBlank, TRUE));
      WriteInteger(CSection, 'spaceperindent', SpacePerIndent);
      WriteString(CSection, 'feedroundbegin', GetEnumName(TypeInfo(TFeedBegin), Ord(FeedRoundBegin)));
      WriteString(CSection, 'feedbeforeend', BoolToStr(FeedBeforeEnd, TRUE));
      WriteString(CSection, 'feedafterthen', BoolToStr(FeedAfterThen, TRUE));
      WriteString(CSection, 'exceptsingle', BoolToStr(ExceptSingle, TRUE));
      WriteString(CSection, 'feedaftervar', BoolToStr(FeedAfterVar, TRUE));
      WriteString(CSection, 'feedeachunit', BoolToStr(FeedEachUnit, TRUE));
      WriteString(CSection, 'nofeedbeforethen', BoolToStr(NoFeedBeforeThen, TRUE));
      WriteString(CSection, 'feedelseif', BoolToStr(FeedElseIf, TRUE));
      WriteString(CSection, 'fillnewwords', GetEnumName(TypeInfo(TFillMode), Ord(FillNewWords)));
      WriteString(CSection, 'feedaftersemicolon', BoolToStr(FeedAfterSemiColon, TRUE));
      WriteString(CSection, 'startcommentout', StartCommentOut);
      WriteString(CSection, 'endcommentout', EndCommentOut);
      WriteString(CSection, 'wraplines', BoolToStr(WrapLines, TRUE));
      WriteInteger(CSection, 'wrapposition', WrapPosition);
      WriteInteger(CSection, 'aligncommentpos', AlignCommentPos);
      WriteString(CSection, 'aligncomments', BoolToStr(AlignComments, TRUE));
      WriteInteger(CSection, 'alignvarpos', AlignVarPos);
      WriteString(CSection, 'alignvar', BoolToStr(AlignVar, TRUE));
      WriteString(CSection, 'coperators', BoolToStr(SupportCOperators, TRUE));
    end;
    UpdateFile;
  finally
    Free;
  end;
end;

procedure IniLoadSettings(const ASettings: PSettings; const AFileName: TFileName);
begin
  LLog.Append(Format('IniLoadSettings(,%s)', [AFileName]));
  with TIniFile.Create(AFileName) do
  try
    with ASettings^ do
    begin
      //Shortcut := ReadInteger(CSection, 'shortcut', 0);
      SpaceOperators := TSpaceBefore(GetEnumValue(TypeInfo(TSpaceBefore), ReadString(CSection, 'spaceoperators', 'spBoth')));
      SpaceColon := TSpaceBefore(GetEnumValue(TypeInfo(TSpaceBefore), ReadString(CSection, 'spacecolon', 'spAfter')));
      SpaceSemiColon := TSpaceBefore(GetEnumValue(TypeInfo(TSpaceBefore), ReadString(CSection, 'spacesemicolon', 'spAfter')));
      SpaceComma := TSpaceBefore(GetEnumValue(TypeInfo(TSpaceBefore), ReadString(CSection, 'spacecomma', 'spAfter')));
      SpaceLeftBr := TSpaceBefore(GetEnumValue(TypeInfo(TSpaceBefore), ReadString(CSection, 'spaceleftbr', 'spNone')));
      SpaceRightBr := TSpaceBefore(GetEnumValue(TypeInfo(TSpaceBefore), ReadString(CSection, 'spacerightbr', 'spNone')));
      SpaceLeftHook := TSpaceBefore(GetEnumValue(TypeInfo(TSpaceBefore), ReadString(CSection, 'spacelefthook', 'spNone')));
      SpaceRightHook := TSpaceBefore(GetEnumValue(TypeInfo(TSpaceBefore), ReadString(CSection, 'spacerighthook', 'spNone')));
      SpaceEqualOper := TSpaceBefore(GetEnumValue(TypeInfo(TSpaceBefore), ReadString(CSection, 'spaceequaloper', 'spBoth')));
      UpperCompDirectives := StrToBool(ReadString(CSection, 'uppercompdirectives', 'True'));
      UpperNumbers := StrToBool(ReadString(CSection, 'uppernumbers', 'True'));
      ReservedCase := TCase(GetEnumValue(TypeInfo(TCase), ReadString(CSection, 'reservedcase', 'rfLowerCase')));
      StandDirectivesCase := TCase(GetEnumValue(TypeInfo(TCase), ReadString(CSection, 'standdirectivescase', 'rfLowerCase')));
      ChangeIndent := StrToBool(ReadString(CSection, 'changeindent', 'True'));
      NoIndentElseIf := StrToBool(ReadString(CSection, 'noindentelseif', 'False'));
      IndentBegin := StrToBool(ReadString(CSection, 'indentbegin', 'False'));
      IndentTry := StrToBool(ReadString(CSection, 'indenttry', 'False'));
      IndentTryElse := StrToBool(ReadString(CSection, 'indenttryelse', 'False'));
      IndentCaseElse := StrToBool(ReadString(CSection, 'indentcaseelse', 'False'));
      IndentComments := StrToBool(ReadString(CSection, 'indentcomments', 'False'));
      IndentCompDirectives := StrToBool(ReadString(CSection, 'indentcompdirectives', 'False'));
      BlankProc := StrToBool(ReadString(CSection, 'blankproc', 'True'));
      BlankSubProc := StrToBool(ReadString(CSection, 'blanksubproc', 'False'));
      RemoveDoubleBlank := StrToBool(ReadString(CSection, 'removedoubleblank', 'False'));
      SpacePerIndent := ReadInteger(CSection, 'spaceperindent', 2);
      FeedRoundBegin := TFeedBegin(GetEnumValue(TypeInfo(TFeedBegin), ReadString(CSection, 'feedroundbegin', 'NewLine')));
      FeedBeforeEnd := StrToBool(ReadString(CSection, 'feedbeforeend', 'False'));
      FeedAfterThen := StrToBool(ReadString(CSection, 'feedafterthen', 'False'));
      ExceptSingle := StrToBool(ReadString(CSection, 'exceptsingle', 'False'));
      FeedAfterVar := StrToBool(ReadString(CSection, 'feedaftervar', 'False'));
      FeedEachUnit := StrToBool(ReadString(CSection, 'feedeachunit', 'False'));
      NoFeedBeforeThen := StrToBool(ReadString(CSection, 'nofeedbeforethen', 'False'));
      FeedElseIf := StrToBool(ReadString(CSection, 'feedelseif', 'False'));
      FillNewWords := TFillMode(GetEnumValue(TypeInfo(TFillMode), ReadString(CSection, 'fillnewwords', 'fmUnchanged')));
      FeedAfterSemiColon := StrToBool(ReadString(CSection, 'feedaftersemicolon', 'False'));
      StrPCopy(StartCommentOut, ReadString(CSection, 'startcommentout', '{(*}'));
      StrPCopy(EndCommentOut, ReadString(CSection, 'endcommentout', '{*)}'));
      WrapLines := StrToBool(ReadString(CSection, 'wraplines', 'False'));
      WrapPosition := ReadInteger(CSection, 'wrapposition', 81);
      AlignCommentPos := ReadInteger(CSection, 'aligncommentpos', 40);
      AlignComments := StrToBool(ReadString(CSection, 'aligncomments', 'False'));
      AlignVarPos := ReadInteger(CSection, 'alignvarpos', 20);
      AlignVar := StrToBool(ReadString(CSection, 'alignvar', 'False'));
      SupportCOperators := StrToBool(ReadString(CSection, 'coperators', 'False'));
    end;
  finally
    Free;
  end;
end;

initialization
  LLog := TLog.Create(CLogName);
  
finalization
  LLog.Free;
  
end.

