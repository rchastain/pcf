
const
  CAppName = 'PCF (Pascal Code Formatter)';
  CVersion = '0.0.1';
  CAuthor  = 'Egbert van Nes';
  CContributors = 'Uwe Schächterle, Roland Chastain';
  CBuild = 'build ' + {$I %DATE%} + ' ' + {$I %TIME%};
  CCompiler = 'Free Pascal ' + {$I %FPCVERSION%} + ' ' + {$I %FPCTARGETOS%} + ' ' + {$I %FPCTARGET%};
  CAppInfo = CAppName + ' v' + CVersion + ' ' + CBuild + ' ' + CCompiler;
