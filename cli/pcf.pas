
program pcf;

{$MODE objfpc}{$H+}

uses
{$IFDEF UNIX}
  CThreads,
{$ENDIF}
  Classes, SysUtils, CustApp, StrUtils,
{$IFDEF UseUTF8Tools}
  CharEncStreams,
{$ENDIF}
  DelForTypes, DelForEngine, Settings;

{$I version.inc}

function FormatText(const AText: string; const AConf: TFileName): string;
var
  LSettings: PSettings;
  LChar: PChar;
begin
  New(LSettings);
  LSettings^ := DefaultSettings(TRUE);
  
  if FileExists(AConf) then
  begin
    if ExtractFileExt(AConf) = '.ini' then
      IniLoadSettings(LSettings, AConf)
    else
      LoadSettings(LSettings, AConf);
  end else
  begin
    if Length(AConf) > 0 then
      WriteLn(ErrOutput, Format('File not found: %s', [AConf]));
    WriteLn('Using default settings.');    
  end;

  if Assigned(DelForParser) then
    DelForParser.Free;
  DelForParser := TDelForParser.Create;
  
  try
    DelForParser.Clear;
    LChar := PChar(AText);
    DelForParser.Text := LChar;
    DelForParser.Settings := LSettings^;
    if not DelForParser.Parse then
      WriteLn(ErrOutput, 'Error while parsing settings.');
    LChar := DelForParser.Text;
    result := StrPas(LChar);
  finally
   DelForParser.Free;
   DelForParser := nil;
  end;
  
  Dispose(LSettings);
end;

procedure FormatFile(const AIn, AOut, AConf: TFileName);
var
{$IFDEF UseUTF8Tools}
  LStream: TCharEncStream;
  LText: string;
{$ELSE}
  LList: TStringList;
{$ENDIF}
  LBackupFile: TFileName;
begin
  LBackupFile := FormatDateTime('YYMMDDhhnnsszzz".bak"', Now);
{$IFDEF UseUTF8Tools}
  LStream := TCharEncStream.Create;
  LStream.LoadFromFile(AIn);
  LStream.SaveToFile(LBackupFile);
  LText := LStream.UTF8Text;
  LText := FormatText(LText, AConf); // <---
  LStream.UTF8Text := LText;
  if Length(AOut) > 0 then
    LStream.SaveToFile(AOut)
  else
    Write(LText);
  LStream.Free;
{$ELSE}
  LList := TStringList.Create;
  LList.LoadFromFile(AIn);
  LList.SaveToFile(LBackupFile);
  LList.Text := FormatText(LList.Text, AConf);
  if Length(AOut) > 0 then
    LList.SaveToFile(AOut)
  else
    Write(LList.Text);
  LList.Free;
{$ENDIF}
end;

type
  TPascalFormatter = class(TCustomApplication)
  protected
    procedure DoRun; override;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  end;

procedure TPascalFormatter.DoRun;
const
  CStdConf = 'pcf.cfg';
var
  LErr: string;
  LIn: TStringArray;
  LOut, LConf: TFileName;
  LConfArr: array[0..3] of TFileName;
  i: integer;
begin
  inherited DoRun;
  WriteLn(CAppInfo);  
  LErr := CheckOptions('c:ho:', 'conf: help out:');
  if Length(LErr) > 0 then
  begin
    WriteLn(ErrOutput, 'Options checking failed: ', LErr);
    WriteHelp;
  end else
  begin
    if HasOption('h', 'help') then
      WriteHelp
    else
    begin
      LIn := GetNonOptions('c:ho:', ['conf:', 'help', 'out:']);
      if Length(LIn) = 1 then
      begin
        WriteLn(Format('Input file: %s', [LIn[0]]));        
        if FileExists(LIn[0]) then
        begin        
          if HasOption('o', 'out') then
          begin
            LOut := GetOptionValue('o', 'out');
          end else
          begin
            WriteLn('No output file specified.');
            (*
            LOut := Format('%s_%s%s', [ChangeFileExt(ExtractFileName(LIn[0]), ''), FormatDateTime('YYYYMMDDhhnnsszzz', Now), ExtractFileExt(LIn[0])]);
            *)
            LOut := EmptyStr;
          end;
          WriteLn(Format('Output file: %s', [IfThen(Length(LOut) > 0, LOut, 'StdOut')]));  
          
          if HasOption('c', 'conf') then
            LConf := GetOptionValue('c', 'conf')
          else
          begin
            WriteLn('No configuration file specified.');
            
            (* Search pcf.cfg configuration file in current directory. *)
            LConfArr[0] := IncludeTrailingPathDelimiter(GetCurrentDir) + CStdConf;
            (* Search pcf.ini configuration file in current directory. *)
            LConfArr[1] := ChangeFileExt(LConfArr[0], '.ini');
            (* Search pcf.cfg configuration file in application directory. *)
            LConfArr[2] := IncludeTrailingPathDelimiter(ExtractFilePath(ExeName)) + CStdConf;
            (* Search pcf.ini configuration file in application directory. *)
            LConfArr[3] := ChangeFileExt(LConfArr[2], '.ini');
            
            i := 0;
            SetLength(LConf, 0);
            while (i <= 3) and (Length(LConf) = 0) do
            begin
              WriteLn(Format('Search %s', [LConfArr[i]]));
              if FileExists(LConfArr[i]) then
              begin
                LConf := LConfArr[i];
                WriteLn(Format('Found configuration file: %s', [LConf]));
              end else
                Inc(i);
            end;
          end;
          if Length(LConf) > 0 then
            WriteLn(Format('Configuration file: %s', [LConf]))
          else
            WriteLn('No configuration file.');
          
          FormatFile(LIn[0], LOut, LConf); // <---
        end else
          WriteLn(ErrOutput, Format('File does not exist: "%s"', [LIn[0]]));       
      end else
      begin
        if Length(LIn) = 0 then
          WriteLn(ErrOutput, 'Input file not specified.')
        else
          WriteLn(ErrOutput, 'More than one input file specified.');
        WriteHelp;
      end;
    end;
  end;
  Terminate;
end;

constructor TPascalFormatter.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException := True;
end;

destructor TPascalFormatter.Destroy;
begin
  inherited Destroy;
end;

procedure TPascalFormatter.WriteHelp;
var
  LExeName: string;
begin
  LExeName := ExtractFileName(ExeName);
  WriteLn('Usage:');
  WriteLn('  ', LExeName, ' <filename> -o <file>');
  WriteLn('  ', LExeName, ' <filename> --out=<file>');
  WriteLn('  ', LExeName, ' <filename> -o <file> -c <file>');
  WriteLn('  ', LExeName, ' <filename> --out=<file> --conf=<file>');
  WriteLn('  ', LExeName, ' -h');
  WriteLn('  ', LExeName, ' --help');
  WriteLn('Notes:');
  WriteLn('  * A copy of the input file is automatically saved in current directory, under the name YYMMDDhhnnsszzz.bak.');
  WriteLn('  * If no output file is specified, the formatted code is written to standard output.');
  WriteLn('  * The configuration file can be or a binary file or a INI text file (*.ini). Both can be created from the settings editor.');
end;

var
  LFormatter: TPascalFormatter;

{$R *.res}

begin
  LFormatter := TPascalFormatter.Create(nil);
  LFormatter.Title := 'Pascal Formatter';
  LFormatter.Run;
  LFormatter.Free;
end.
